<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Historywork extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
