<?php

namespace App\Http\Controllers;

use App\Price;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class PriceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $price = Price::first() ;
        return view('panel.price' , compact('price')) ;


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Price  $price
     * @return \Illuminate\Http\Response
     */
    public function show(Price $price)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Price  $price
     * @return \Illuminate\Http\Response
     */
    public function edit(Price $price)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Price  $price
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Price $price)
    {


        $request->validate([

            'price_ads' =>  'required|max:50' ,
            'price_cv' =>  'required|max:50' ,
            'price_fory' =>  'required|max:50' ,
            'price_vizheh' =>  'required|max:50' ,
            'price_barjasteh' =>  'required|max:50' ,
            'price_nardeban' =>  'required|max:50' ,
            'price_tamdid' =>  'required|max:50' ,
            'price_tamdid_nardeban' =>  'required|max:50' ,

        ]);

        $price_ads= str_replace('', ',', $request->price_ads);
        $price_ads = preg_replace('/[^A-Za-z0-9\-]/', '', $price_ads);

        $price_cv= str_replace('', ',', $request->price_cv);
        $price_cv = preg_replace('/[^A-Za-z0-9\-]/', '', $price_cv);

        $price_fory= str_replace('', ',', $request->price_fory);
        $price_fory = preg_replace('/[^A-Za-z0-9\-]/', '', $price_fory);

        $price_vizheh= str_replace('', ',', $request->price_vizheh);
        $price_vizheh = preg_replace('/[^A-Za-z0-9\-]/', '', $price_vizheh);

        $price_barjasteh= str_replace('', ',', $request->price_barjasteh);
        $price_barjasteh = preg_replace('/[^A-Za-z0-9\-]/', '', $price_barjasteh);

        $price_nardeban= str_replace('', ',', $request->price_nardeban);
        $price_nardeban = preg_replace('/[^A-Za-z0-9\-]/', '', $price_nardeban);

        $price_tamdid= str_replace('', ',', $request->price_tamdid);
        $price_tamdid = preg_replace('/[^A-Za-z0-9\-]/', '', $price_tamdid);

        $price_tamdid_nardeban= str_replace('', ',', $request->price_tamdid_nardeban);
        $price_tamdid_nardeban = preg_replace('/[^A-Za-z0-9\-]/', '', $price_tamdid_nardeban);



        Price::where('id' , $price->id)->update(['price_ads' => $price_ads, 'price_cv' => $price_cv, 'price_fory' => $price_fory, 'price_vizheh' => $price_vizheh, 'price_fory_nardeban' => $price_barjasteh, 'price_nardeban' =>  $price_nardeban, 'price_tamdid' => $price_tamdid, 'price_tamdid_nardeban' => $price_tamdid_nardeban ]);
        Session::flash('status','با موفقیت ویرایش شد');
        return redirect()->back();



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Price  $price
     * @return \Illuminate\Http\Response
     */
    public function destroy(Price $price)
    {
        //
    }
}
