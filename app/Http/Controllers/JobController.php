<?php

namespace App\Http\Controllers;

use App\Job;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class JobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $jobs = Job::paginate(16);
        return view('panel.list_job' , compact('jobs')) ;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('panel.add_job') ;

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([

            'Job_name'=> 'required',

        ]);


        $Job = new Job();
        $Job->Job_name = $request->Job_name;
        $Job->save();

        Session::flash('status','با موفقیت ثبت شد');
        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Job  $Job
     * @return \Illuminate\Http\Response
     */
    public function show(Job $job)
    {

        return view('panel.edit_Job' , compact('job')) ;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Job  $Job
     * @return \Illuminate\Http\Response
     */
    public function edit(Job $job)
    {



    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Job  $Job
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Job $job)
    {



        $request->validate([

            'job_name'=> 'required'

        ]);


        Job::where('id' , $job->id)->update(['job_name' => $request->job_name ]) ;
        Session::flash('status','با موفقیت ویرایش شد');
        return redirect('/job');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Job  $Job
     * @return \Illuminate\Http\Response
     */
    public function destroy(Job $job)
    {


        Job::where('id' , '=' , $job->id)->delete();

        Session::flash('status', 'با موفقیت حذف شد');
        return redirect('/job');


    }
}
