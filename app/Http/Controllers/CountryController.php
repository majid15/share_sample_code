<?php

namespace App\Http\Controllers;

use App\City;
use App\Country;
use App\State;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function ajax_change_state(Request $request)
    {

        $states = State::where('country_id' , $request->value)->get() ;
        $option = "" ;
        $option1 = "" ;
        $city_name = "" ;
        $name_state = "" ;


        $i = 0 ;
        $j = 0 ;
        foreach ($states as $state){
            if($i == 0 ) {
                $name_state = $state->state_name ;
                   $citys = City::where('state_id' , $state->id)->get() ;
                    foreach ($citys as $city){

                        if($j == 0 ) {

                            $city_name = $city->city_name ;

                        }

                        $appender =   '<option class="remove_city" value="'.$city->id.'" >  '.$city->city_name.'  </option>'  ;
                        $option1 = $option1 .  $appender ;


                        $j ++ ;


                    }

            }
            $appender =   '<option class="remove_state" value="'.$state->id.'" >  '.$state->state_name.'  </option>'  ;
            $option = $option .  $appender ;


            $i ++ ;
        }


        return response()->json(['success'=> 'تغییر وضعیت با موفقیت انجام شد' , 'option' => $option, 'option1' => $option1,'name_state' => $name_state ,'city_name' => $city_name  ]);



    }




      public function ajax_change_city(Request $request)
    {

        $citys = City::where('state_id' , $request->value)->get() ;
        $option = "" ;
        $city_name = "" ;

        $i = 0 ;
        foreach ($citys as $city){

            if($i == 0 ) {

                $city_name = $city->city_name ;

            }
            $appender =   '<option class="remove_city" value="'.$city->id.'" >  '.$city->city_name.'  </option>'  ;
            $option = $option .  $appender ;



            $i ++ ;
        }


        return response()->json(['success'=> 'تغییر وضعیت با موفقیت انجام شد' , 'option' => $option , 'city_name' => $city_name ]);



    }




    public function index()
    {

        $countrys = Country::paginate(16) ;
        return view('panel.list_country' , compact('countrys')) ;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('panel.add_country') ;

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $request->validate([

            'country_name'=> 'required',

        ]);


        $country = new Country();
        $country->country_name = $request->country_name;
        $country->save();

        Session::flash('status','با موفقیت ثبت شد');
        return redirect()->back();


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function show(Country $country)
    {

        return view('panel.edit_country' , compact('country')) ;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function edit(Country $country)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Country $country)
    {

        $request->validate([

            'country_name'=> 'required'

        ]);


        Country::where('id' , $country->id)->update(['country_name' => $request->country_name ]) ;
        Session::flash('status','با موفقیت ویرایش شد');
        return redirect('/country');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function destroy(Country $country)
    {
        Country::where('id' , '=' , $country->id)->delete();

        Session::flash('status', 'با موفقیت حذف شد');
        return redirect('/country');
    }
}
