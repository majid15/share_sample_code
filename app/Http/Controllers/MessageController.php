<?php

namespace App\Http\Controllers;

use App\Message;
use App\Messageread;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('site.payamha');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'description' => 'required'
         ]);

        if($request->description != null ) {


            $description = str_replace("\\", "", $request->description);
            $description = str_replace("xheditor-1.2.1", "/xheditor-1.2.1", $description);
            $description = str_replace("//", "/", $description);


            $message = new Message() ;
            $message->user_id = Auth::user()->id  ;
            $message->message = $description  ;
            $message->save()  ;


            Session::flash('status','با موفقیت ثبت شد');
            return redirect()->back() ;


        }


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function ajax_read_message(Request $request)
    {

       $messages = Message::where('user_id' , Auth::user()->id)->get() ;
       $messagereads = Messageread::get() ;

       foreach ($messages as $message) {
           $exist = 0 ;
         foreach ($messagereads as $messageread) {

             if($message->id == $messageread->message_id) { $exist = 1 ; }

           }

         if($exist == 0) {

            $messageread = new Messageread() ;
            $messageread->user_id = Auth::user()->id ;
            $messageread->message_id = $message->id ;
            $messageread->save() ;

           }

        }



    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function edit(Message $message)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Message $message)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function destroy(Message $message)
    {
        //
    }
}
