<?php

namespace App\Http\Controllers;

use App\Citywant;
use App\Historywork;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class HistoryworkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ajax_remove_history(Request $request)
    {

        Historywork::where('id' , $request->id)->delete() ;


        return response()->json(['success'=> 'تغییر وضعیت با موفقیت انجام شد' ]);


    }


    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('site.historywork'  ) ;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([

            'title' =>  'required' ,
            'companyname' =>  'required' ,

        ]);

        if($request->its_work != null){

            $year_end = null  ;
            $monthend = null  ;

        }else{

            $year_end = $request->year_end  ;
            $monthend = $request->month_end  ;

        }




        $historywork = new Historywork();
        $historywork->user_id = Auth::user()->id ;
        $historywork->title = $request->title ;
        $historywork->name_company = $request->companyname ;
        $historywork->yearstart = $request->year_start ;
        $historywork->monthstart = $request->month_start ;
        $historywork->yearend = $year_end;
        $historywork->monthend = $monthend ;
        $historywork->description = $request->description ;

        $historywork->save() ;

        Session::flash('status','با موفقیت ثبت شد');
        return redirect()->back() ;

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Historywork  $historywork
     * @return \Illuminate\Http\Response
     */
    public function show(Historywork $historywork)
    {


        return view('site.edit_historywork' , compact('historywork'));


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Historywork  $historywork
     * @return \Illuminate\Http\Response
     */
    public function edit(Historywork $historywork)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Historywork  $historywork
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Historywork $historywork)
    {


        $request->validate([

            'title' =>  'required' ,
            'companyname' =>  'required' ,

        ]);

        if($request->its_work != null){

            $year_end = null  ;
            $monthend = null  ;

        }else{

            $year_end = $request->year_end  ;
            $monthend = $request->month_end  ;

        }



        Historywork::where('id' , $historywork->id)->update([
            'title' => $request->title
            , 'name_company' => $request->companyname
            , 'yearstart' =>  $request->year_start
            , 'monthstart' =>  $request->month_start
            , 'yearend' =>  $year_end
            , 'monthend' =>  $monthend
            , 'description' =>  $request->description
        ]) ;


        Session::flash('status','با موفقیت اپدیت شد');
        return redirect()->back() ;



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Historywork  $historywork
     * @return \Illuminate\Http\Response
     */
    public function destroy(Historywork $historywork)
    {
        //
    }
}
