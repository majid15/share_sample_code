<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Trez\RayganSms\Facades\RayganSms;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        $this->user = new User;
    }

    public function login(Request $request)
    {

        $code_session =  Session::get('code') ;
        $phone =  Session::get('phone') ;

        // Check validation
        $this->validate($request, [
            'code' => 'required|in:'.$code_session,

        ],
            [
                'code.in' => 'کد وارد شده صحیح نیست',
            ]);

        // Get user record


        $user = User::where('phone', $phone)->first() ;
        $user_count = User::where('phone', $phone)->get()->count();

        // Check Condition Mobile No. Found or Not
        if($user_count == 0) {



            $user_info =  User::create([

                'phone' => $phone ,
                'password' => Hash::make('majid123')
            ]);


            $true = true ;

            while($true == true) {

                $code = rand(10001,99999) ;
                $user_identifie = User::where('identifiercode' , $code)->get()->count() ;

                if($user_identifie == 0 ){
                    User::where('id' , $user_info->id)->update(['identifiercode' => $code])  ;
                    $true = false ;

                }



            }

            RayganSms::sendAuthCode($phone, 'کاربر گرامی کد معرف شما در کارکو : '.$code.' میباشد ', false);
            Session::flash('status','با موفقیت وارد شد');
            Auth::login($user_info);
            return redirect('/register_');
        }else{



            if ($user->name == null || $user->name == "" ){

                Auth::login($user);
                Session::flash('status','با موفقیت وارد شد');
                return redirect('/register_');

            }else{
                Auth::login($user);
                Session::flash('status','با موفقیت وارد شد');
                if (Session::get('link') == "http://kaarko.ir/logout" || Session::get('link') == "http://kaarko.ir/log_out" || Session::get('link') == "https://kaarko.ir/logout" || Session::get('link') == "https://kaarko.ir/log_out"){

                    return redirect("/");

                }else{

                    return redirect("/");

                }

            }



        }










    }

}
