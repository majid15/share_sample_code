<?php

namespace App\Http\Controllers;

use App\Advertise;
use App\Cv;
use App\Jobwant;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class CvController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function list_cv(Advertise $advertise)
    {

        $cvs =  Cv::join('jobwants','cvs.user_id','=','jobwants.user_id')
                ->select('jobwants.*' , 'cvs.*')
                ->where('cvs.country' , $advertise->country_id)
                ->where('cvs.state' , $advertise->state_id)
                ->where('cvs.city' , $advertise->city_id)
                ->where('jobwants.job_id' , $advertise->job_id)
                ->where('jobwants.sub_job_id' , $advertise->sub_job_id)
                ->get() ;




        return view('site.list_cv' , compact('cvs' , 'advertise' )) ;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('site.cv');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $request->validate([

            'email' =>  'required' ,
            'title' =>  'required|max:30' ,

        ]);

            User::where('id' , Auth::user()->id)->update(['email' => $request->email]) ;
            $count_cv = Cv::where('user_id' , Auth::user()->id)->get()->count() ;
            $code = rand(10001,99999) ;
            $showcv = $request->showcv ;

            if($showcv == null) {

                $showcv = 0 ;

            }else{

                $showcv = 1 ;
            }

        if($count_cv == 0 ) {

        $cv = new Cv() ;
        $cv->user_id = Auth::user()->id ;
        $cv->showcv = $showcv ;
        $cv->title = $request->title ;
        $cv->grade = $request->grade ;
        $cv->married = $request->married ;
        $cv->country = $request->country ;
        $cv->state = $request->state ;
        $cv->city = $request->city ;
        $cv->soldier = $request->soldier ;
        $cv->age = (int)$request->age ;
        $cv->education =  $request->madrak ;
        $cv->salary =  (int)$request->salary ;
        $cv->kind1 =  $request->kind1 ;
        $cv->kind2 =  $request->kind2 ;
        $cv->kind3 =  $request->kind3 ;
        $cv->kind4 =  $request->kind4 ;
        $cv->description =  $request->description ;
        $cv->url = $request->title." ".Auth::user()->name." ".Auth::user()->country." ".Auth::user()->state." ".Auth::user()->city." ".time().$code;
        $cv->keyword =Auth::user()->name." ".$request->title." , ".Auth::user()->state." , ".Auth::user()->city." , ".$request->abzar.",  کاربین , ;hvfdk , kaarbin" ;
        $cv->save() ;
        Session::flash('status','با موفقیت ثبت شد');

        }else{

            Cv::where('user_id', Auth::user()->id)->update([
                 'title' => $request->title
                , 'showcv' => $showcv
                , 'grade' => $request->grade
                , 'married' => $request->married
                , 'country' => $request->country
                , 'state' => $request->state
                , 'city' => $request->city
                , 'soldier' => $request->soldier
                , 'age' => (int)$request->age
                , 'education' => $request->madrak
                , 'salary' => (int)$request->salary
                , 'kind1' => $request->kind1
                , 'kind2' => $request->kind2
                , 'kind3' => $request->kind3
                , 'kind4' => $request->kind4
                , 'description' => $request->description
                , 'url' => $request->title." ".Auth::user()->name." ".Auth::user()->country." ".Auth::user()->state." ".Auth::user()->city." ".time().$code
                , 'keyword' => Auth::user()->name." ".$request->title." , ".Auth::user()->state." , ".Auth::user()->city." , ".$request->abzar.",  کاربین , ;hvfdk , karbin"

            ]) ;

            Session::flash('status','با موفقیت ویرایش شد');

        }



        return redirect()->back() ;

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Cv  $cv
     * @return \Illuminate\Http\Response
     */
    public function show(Cv $cv)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Cv  $cv
     * @return \Illuminate\Http\Response
     */
    public function edit(Cv $cv)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Cv  $cv
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cv $cv)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Cv  $cv
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cv $cv)
    {
        //
    }
}
