<?php

namespace App\Http\Controllers;

use App\City;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CityController extends Controller
{





    public function ajax_remove_city_session(Request $request)
    {

        $product_citys = Session::get('city');
        foreach ($product_citys as $key => $item) {

            if ($item == $request->id) {

                $product_citys->pull($key);

            }

        }


        $empty = 0 ;
        foreach ($product_citys as $key => $item) {
            $empty = 1 ;
        }

        if($empty == 0) {

            Session::forget('city');

        }else{

            Session::put('city', $product_citys);

        }




        return response()->json(['success' => 'اضافه']);

    }







    public function index()
    {

        $citys = City::paginate(16) ;
        return view('panel.list_city' , compact('citys')) ;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('panel.add_city') ;

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $request->validate([

            'city_name'=> 'required',
            'country'=> 'required',
            'state'=> 'required',

        ]);


        $city = new City();
        $city->country_id = $request->country;
        $city->state_id = $request->state;
        $city->city_name = $request->city_name;
        $city->save();

        Session::flash('status','با موفقیت ثبت شد');
        return redirect()->back();


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function show(City $city)
    {

        return view('panel.edit_city' , compact('city')) ;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function edit(City $city)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, City $city)
    {

        $request->validate([

            'city_name'=> 'required',
            'country'=> 'required',
            'state'=> 'required',

        ]);


        City::where('id' , $city->id)->update(['city_name' => $request->city_name ,  'country_id' => $request->country , 'state_id' => $request->state ]) ;
        Session::flash('status','با موفقیت ویرایش شد');
        return redirect('/city');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\City  $city
     * @return \Illuminate\Http\Response
     */
    public function destroy(City $city)
    {
        City::where('id' , '=' , $city->id)->delete();

        Session::flash('status', 'با موفقیت حذف شد');
        return redirect('/city');
    }
















    public function ajax_save_or_remove_city_in_session(Request $request)
    {

        $check_exist = 0 ;

        if (Session::has('state')) {

            $state = Session::get('state');
            $check =  $state->contains($request->id_state) ;

            if($check == 1 ) {

                foreach ($state as $key => $item) {
                    if ($item == $request->id_state ) {
                        $state->pull($key);
                    }
                }


                $empty = 0 ;
                foreach ($state as $key => $item) {
                    $empty = 1 ;
                }

                if($empty == 0) {

                    Session::forget('state');

                }else{

                    Session::put('state', $state);

                }




            }
        }

        if (!Session::has('city')){

            if($request->status == "1" ) {
                $product =collect([]);
                $product->push($request->id);
                Session::put('city', $product);
                return response()->json(['success'=> 'اضافه' ]);
            }else{

                return response()->json(['success'=> 'اضافه' ]);
            }

        }else{

            $product = Session::get('city');
            foreach ($product as $key => $item) {
                if ($item == $request->id ) {
                    $check_exist = 1 ;
                    $get_key = $key ;
                }
            }

            if($check_exist == 1) {
                if($request->status == "0" ) {
                    $product->pull($get_key);

                    $empty = 0 ;
                    foreach ($product as $key => $item) {
                        $empty = 1 ;
                    }

                    if($empty == 0) {

                        Session::forget('city');

                    }else{

                        Session::put('city', $product);

                    }




                    Session::put('city', $product);
                    return response()->json(['success' => 'حذف']);
                }else{
                    return response()->json(['success' => 'حذف']);
                }
            }else{

                if($request->status == "1" ) {
                    $product->push($request->id);
                    Session::put('city', $product);
                    return response()->json(['success' => 'اضافه']);
                }else{

                    return response()->json(['success' => 'اضافه']);

                }
            }

        }





//  return  Session::get('state');
//        $collection = collect([10,2,3]);
//        $collection->push(1);
//        $collection->push(4);
//        $collection->push(6);
//
//
//        foreach ($collection as $key => $item) {
//            if ($item == 1 ) {
//                $collection->pull($key);
//            }
//        }
//       $check =  $collection->contains(1);
//       return  $collection ;

//        foreach ($collection as $key => $item) {
//            if ($item == 1 ) {
//               $collection->pull($key);
//            }
//        }
        // $check =  $collection->contains(1);




    }










}
