<?php

namespace App\Http\Controllers;

use App\Advertise;
use App\Favorite;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FavoriteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ajax_add_to_favorite(Request $request)
    {
        if(Auth::check()) {

        $fav = new Favorite() ;
        $fav->user_id = Auth::user()->id ;
        $fav->advertise_id = $request->id ;
        $fav->save() ;

        return response()->json(['success'=> 'موفق' ]);
        }else{

            return response()->json(['success'=> 'ابتدا میبایست وارد شوید !!!' ]);

        }


    }

  public function ajax_remove_to_favorite(Request $request)
    {

        if(Auth::check()) {
        Favorite::where('user_id' , Auth::user()->id)->where('advertise_id' , $request->id)->delete() ;

        return response()->json(['success'=> 'موفق' ]);
        }else{

            return response()->json(['success'=> 'ابتدا میبایست وارد شوید !!!' ]);

        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Favorite  $favorite
     * @return \Illuminate\Http\Response
     */
    public function show(Favorite $favorite)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Favorite  $favorite
     * @return \Illuminate\Http\Response
     */
    public function edit(Favorite $favorite)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Favorite  $favorite
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Favorite $favorite)
    {
        //
    }



    public function favorite(Request $request)
    {


        $advertises =  Advertise::join('favorites','advertises.id','=','favorites.advertise_id')
            ->select('advertises.*')
            ->where('favorites.user_id' , Auth::user()->id  )
            ->get() ;


        $favorits = Favorite::where('user_id' , Auth::user()->id)->get() ;

        $artilces = '';
        if ($request->ajax()) {



            foreach ($advertises as $advertise) {
                $check_fav = 0 ;
                if(Auth::check()) {

                    foreach ($favorits as $favorit) {

                        if($favorit->advertise_id == $advertise->id) {
                            $check_fav = 1 ;
                        }

                    }


                }

                if($check_fav == 0 ) {


                    $i_spam =   ' <i id="remover_fav'.$advertise->id.'" class="fa fa-bookmark-o" ></i> ';

                }else{

                    $i_spam =   ' <i id="remover_fav'.$advertise->id.'" class="fa fa-bookmark" ></i> ';

                }


                if($advertise->statustag_fory != null ) { $status_advertise = '<div class="ribbon-featured"><div class="ribbon-start"></div><div class="ribbon-content">آگهی ویژه</div><div class="ribbon-end"><figure class="ribbon-shadow"></figure></div></div>' ;   ; }else{$status_advertise = "" ;}
                if($advertise->statustag_vizheh == null ) { $color = "#fff" ; }else{$color = "#ff9595" ; }
                $job_name = \App\Job::find($advertise->job_id) ;
                $subjob_name = \App\SubJob::find($advertise->sub_job_id) ;



                $country_name = \App\Country::find($advertise->country_id) ;
                $state_name = \App\State::find($advertise->state_id) ;
                $city_name = \App\City::find($advertise->city_id) ;

                $v2 = verta( $advertise->created_at);
                $v2 = $v2->diffSeconds();

                if($v2 < 300 ) { $v2 = "دقایقی پیش" ;}
                elseif ($v2 > 300 && $v2 < 3600){
                    $v2 = intdiv((int)$v2, 60)   ;
                    $v2 = $v2." دقیه پیش "  ;
                }
                elseif ($v2 > 3600 && $v2 < 86400){
                    $v2 = intdiv((int)$v2, 3600)  ;
                    $v2 = $v2. " ساعت پیش "   ;
                }elseif( $v2 > 86400 && $v2 < 604800){

                    $v2 = intdiv((int)$v2, 86400)  ;
                    $v2 = $v2 . " روز پیش "   ;

                }else{

                    $v2 = intdiv((int)$v2, 604800)  ;
                    $v2 = $v2." هفته پیش "    ;

                }


                $artilces.='


                                                                     <li class="box-style-1 item col-lg-6 col-md-6 col-sm-12 col-xs-12 product type-product post-147 status-publish">

                                                                       '.$status_advertise.'
                                                                        <div class="wrapper">
                                                                            <span class="favorite" id="append_fav'.$advertise->id.'" >

                                                                            '.$i_spam.'

                                                                            </span>
                                                                            <span class="ad_visit">بازدید : '.$advertise->bazdid.'</span>
                                                                            <div style="height: 6rem;">

                                                                                <h3 style="padding-top: 28px !important;position: inherit;border-color: #cacaca;background: #f2f2f2;color: black">'.$advertise->title_ads.'</h3>

                                                                                <a role="button" title="'.$advertise->title_ads.'/show_detail" class="title"></a>
                                                                             </div>
                                                                             <a href="/advertise/'.$advertise->slug.'/show_detail">
                                                                            <div class="meta" style="background: '.$color.';">

                                                                                <div class="form-group" style="margin-top: 13px;">
                                                                                    <h5 style="line-height: 3rem;"><i style="margin-left: 5px;" class="fa fa-files-o"></i>'.$job_name->job_name.' ، '.$subjob_name->subjob_name.'</h5>
                                                                                    <h5 style="line-height: 3rem;"><i style="margin-left: 5px;" class="fa fa-map-marker  text-warning"></i>'.$country_name->country_name.' / '.$state_name->state_name.' / '.$city_name->city_name.'</h5>
                                                                                    <h5 style="line-height: 3rem;"><i style="margin-left: 5px;" class="fa fa-clock-o"></i>'.$v2.'</h5>
                                                                                </div>


                                                                            </div>
                                                                              </a>
                                                                            <!--end meta-->
                                                                            <!--end description-->
                                                                        </div>

                                                                    </li>


                                                              </a>


                                         ';
            }
            return $artilces;
        }

        return view('site.list_favorite'  );

    }



}
