<?php

namespace App\Http\Controllers;

use App\Optionads;
use Illuminate\Http\Request;

class OptionadsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Optionads  $optionads
     * @return \Illuminate\Http\Response
     */
    public function show(Optionads $optionads)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Optionads  $optionads
     * @return \Illuminate\Http\Response
     */
    public function edit(Optionads $optionads)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Optionads  $optionads
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Optionads $optionads)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Optionads  $optionads
     * @return \Illuminate\Http\Response
     */
    public function destroy(Optionads $optionads)
    {
        //
    }
}
