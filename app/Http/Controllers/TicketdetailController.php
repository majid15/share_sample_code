<?php

namespace App\Http\Controllers;

use App\Ticketdetail;
use Illuminate\Http\Request;

class TicketdetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ticketdetail  $ticketdetail
     * @return \Illuminate\Http\Response
     */
    public function show(Ticketdetail $ticketdetail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ticketdetail  $ticketdetail
     * @return \Illuminate\Http\Response
     */
    public function edit(Ticketdetail $ticketdetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ticketdetail  $ticketdetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ticketdetail $ticketdetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ticketdetail  $ticketdetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ticketdetail $ticketdetail)
    {
        //
    }
}
