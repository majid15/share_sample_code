<?php

namespace App\Http\Controllers;

use App\Job;
use App\Jobwant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class JobwantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function ajax_remove_jobwant(Request $request)
    {

        Jobwant::where('id' , $request->id)->delete() ;


        return response()->json(['success'=> 'تغییر وضعیت با موفقیت انجام شد' ]);


    }



    public function index()
    {
        $jobwants = Jobwant::paginate(16) ;
        return view('site.list_jobwant' , compact('jobwants')) ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('site.job_want'  ) ;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'job' =>  'required' ,
            'subjob' =>  'required' ,
            'year' =>  'required' ,

        ]);

        $jobwant = new Jobwant() ;
        $jobwant->user_id = Auth::user()->id ;
        $jobwant->job_id = $request->job ;
        $jobwant->sub_job_id = $request->subjob ;
        $jobwant->year = $request->year ;
        $jobwant->save();

        Session::flash('status','با موفقیت ثبت شد');
        return redirect()->back() ;


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Jobwant  $jobwant
     * @return \Illuminate\Http\Response
     */
    public function show(Jobwant $jobwant)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Jobwant  $jobwant
     * @return \Illuminate\Http\Response
     */
    public function edit(Jobwant $jobwant)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Jobwant  $jobwant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Jobwant $jobwant)
    {


        $request->validate([
            'job' =>  'required' ,
            'subjob' =>  'required' ,
            'year' =>  'required' ,

        ]);


        Jobwant::where('id' , $jobwant->id)->update(['job_id' => $request->job , 'subjob_id' => $request->subjob , 'year' =>  $request->year ]) ;

        Session::flash('status','با موفقیت اپدیت شد');
        return redirect()->back() ;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Jobwant  $jobwant
     * @return \Illuminate\Http\Response
     */
    public function destroy(Jobwant $jobwant)
    {
        //
    }
}
