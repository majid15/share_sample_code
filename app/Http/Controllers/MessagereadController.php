<?php

namespace App\Http\Controllers;

use App\Messageread;
use Illuminate\Http\Request;

class MessagereadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Messageread  $messageread
     * @return \Illuminate\Http\Response
     */
    public function show(Messageread $messageread)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Messageread  $messageread
     * @return \Illuminate\Http\Response
     */
    public function edit(Messageread $messageread)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Messageread  $messageread
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Messageread $messageread)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Messageread  $messageread
     * @return \Illuminate\Http\Response
     */
    public function destroy(Messageread $messageread)
    {
        //
    }
}
