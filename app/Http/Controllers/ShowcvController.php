<?php

namespace App\Http\Controllers;

use App\Showcv;
use Illuminate\Http\Request;

class ShowcvController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Showcv  $showcv
     * @return \Illuminate\Http\Response
     */
    public function show(Showcv $showcv)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Showcv  $showcv
     * @return \Illuminate\Http\Response
     */
    public function edit(Showcv $showcv)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Showcv  $showcv
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Showcv $showcv)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Showcv  $showcv
     * @return \Illuminate\Http\Response
     */
    public function destroy(Showcv $showcv)
    {
        //
    }
}
