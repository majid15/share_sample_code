<?php

namespace App\Http\Controllers;

use App\Advertise;
use App\City;
use App\Favorite;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AdvertiseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('site.list_advertise');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('site.advertise') ;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $request->validate([

            'title_ads' =>  'required|max:50' ,
            'description_ads' =>  'required|max:1500' ,

        ]);
        $code = rand(10001,99999) ;

        $advertise = new Advertise() ;
        $advertise->user_id = Auth::user()->id ;
        $advertise->job_id = $request->job;
        $advertise->sub_job_id = $request->subjob;
        $advertise->country_id = $request->country1;
        $advertise->state_id = $request->state1;
        $advertise->city_id = $request->city1;
        $advertise->title_ads = $request->title_ads;
        $advertise->description_ads = $request->description_ads;
        $advertise->education =  $request->madrak ;
        $advertise->grade =  $request->grade ;
        $advertise->historywork =  $request->year ;
        $advertise->minage = (int)$request->range_min_age ;
        $advertise->maxage = (int)$request->range_max_age ;
        $advertise->email_contact = $request->email_ads ;
        $advertise->whatsapp_contact = $request->whatsapp_ads ;
        $advertise->phone_contact = Auth::user()->phone ;
        $advertise->message_contact = Auth::user()->phone ;
        $advertise->married = $request->married ;
        $advertise->soldier = $request->soldier ;
        $advertise->kind1 = $request->kind1 ;
        $advertise->kind2 = $request->kind2 ;
        $advertise->kind3 = $request->kind3 ;
        $advertise->kind4 = $request->kind4 ;
        $advertise->remote = $request->farwork ;
        $advertise->minsalary = $request->range_min ;
        $advertise->maxsalary = $request->range_max ;
        $advertise->bimeh = $request->bimeh ;
        $advertise->kindpeyment = $request->kindpeyment ;
        $advertise->url =  $request->title_ads." ".Auth::user()->name." ". $request->country1." ". $request->state1." ".$request->city1." ".time().$code ;
        $advertise->keyword = Auth::user()->name." ".$request->title_ads." , ".$request->state1." , ".$request->city1." , ".$request->abzar.",  کاربین , ;hvfdk , kaarbin" ;
        $advertise->save();




        Session::flash('status','با موفقیت دخیره شد');
        return redirect('/advertise/'.$advertise->id.'/edit') ;



    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Advertise  $advertise
     * @return \Illuminate\Http\Response
     */
    public function show(Advertise $advertise)
    {

        return view('site.advertise_details' , compact('advertise'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Advertise  $advertise
     * @return \Illuminate\Http\Response
     */
    public function edit(Advertise $advertise)
    {

        return view('site.advertise_payment',compact('advertise')) ;

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Advertise  $advertise
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Advertise $advertise)
    {

        $request->validate([

            'title_ads' =>  'required|max:50' ,
            'description_ads' =>  'required|max:1500' ,

        ]);
        $code = rand(10001,99999) ;

        Advertise::where('id' , $advertise->id )->update([
            'job_id' => $request->job ,
            'sub_job_id' => $request->subjob ,
            'country_id' => $request->country1 ,
            'state_id' => $request->state1 ,
            'city_id' => $request->city1 ,
            'title_ads' => $request->title_ads ,
            'description_ads' => $request->description_ads ,
            'education' => $request->madrak ,
            'grade' =>  $request->grade ,
            'historywork' =>  $request->year ,
            'minage' =>  (int)$request->range_min_age ,
            'maxage' =>  (int)$request->range_max_age ,
            'email_contact' => $request->email_ads ,
            'whatsapp_contact' => $request->whatsapp_ads ,
            'phone_contact' => Auth::user()->phone ,
            'message_contact' => Auth::user()->phone ,
            'married' => $request->married ,
            'soldier' => $request->soldier ,
            'kind1' => $request->kind1 ,
            'kind2' => $request->kind2 ,
            'kind3' => $request->kind3 ,
            'kind4' => $request->kind4 ,
            'remote' => $request->farwork ,
            'minsalary' => $request->range_min ,
            'maxsalary' => $request->range_max ,
            'bimeh' => $request->bimeh ,
            'kindpeyment' => $request->kindpeyment ,
            'url' => $request->title_ads." ".Auth::user()->name." ". $request->country1." ". $request->state1." ".$request->city1." ".time().$code ,
            'keyword' => Auth::user()->name." ".$request->title_ads." , ".$request->state1." , ".$request->city1." , ".$request->abzar.",  کاربین , ;hvfdk , kaarbin" ,

        ]) ;


        Session::flash('status','با موفقیت دخیره شد');
        return redirect()->back() ;


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Advertise  $advertise
     * @return \Illuminate\Http\Response
     */







    public function updateleveladvertise(Request $request)
    {

        Advertise::where('id' , $request->id)->update(['status' => $request->level]) ;
        return redirect()->back() ;

    }







    public function filter_ads(Request $request)
    {


        if(Auth::check()) {
            $favorits = Favorite::where('user_id' , Auth::user()->id)->get() ;
        }


        $collection = collect([]);
        $citys = City::get() ;


        $advertises =  Advertise::query();
        $advertises->where('created_at', '<=', Carbon::now()->addMonths(1));
        $advertises->where('status' ,  2) ;

        if( $request->grade != null || $request->grade != ""    ){

            Session::put('grade', $request->grade);

        }

        $advertises->where('grade' ,   Session::get('grade') );


        if(  $request->married != null || $request->married != ""   ){

            Session::put('married', $request->married);

        }

        $advertises->where('married',   Session::get('married') );


        if( $request->soldier != null || $request->soldier != ""  ){

            Session::put('soldier', $request->soldier);

        }

        $advertises->where('soldier',   Session::get('soldier') );



        if( $request->madrak != null || $request->madrak != ""  ){

            Session::put('madrak', $request->madrak);

        }

        $advertises->where('education',   Session::get('madrak') );



        if( $request->year != null || $request->year != "" ){

            Session::put('historywork', $request->year);

        }

        $advertises->where('historywork',   Session::get('historywork') );


        if( $request->range_min_age != null || $request->range_min_age != "" ) {
            Session::put('range_min_age', $request->range_min_age);
            Session::put('range_max_age', $request->range_max_age);


        }

         $advertises->where('minage' , "<=" ,  Session::get('range_min_age'))->where('maxage' , ">=" ,  Session::get('range_max_age')) ;

         if( $request->range_min != null || $request->range_min != ""  ) {
             Session::put('range_min', $request->range_min);
             Session::put('range_max', $request->range_max);

         }

        $advertises->where('minsalary' , "<=" ,  Session::get('range_min'))->where('maxsalary' , ">=" ,  Session::get('range_max')) ;





        if ( Session::has('city') || Session::has('state') ) {

            if (Session::has('city')) {

                $product_city = Session::get('city');

                foreach ($product_city as $key => $item) {

                    $collection->push((int)$item);

                }
            }

            if (Session::has('state')) {

                $state = Session::get('state');

                foreach ($state as $key => $item) {
                    foreach ($citys as $city) {

                        if ($city->state_id == $item) {
                            $collection->push($city->id);
                        }

                    }
                }
            }


            $advertises->whereIn('city_id', $collection);
        }


        $advertises = $advertises->orderBy('id' , 'desc')->paginate(10);



        $artilces = '';
        if ($request->ajax()) {



            foreach ($advertises as $advertise) {
                $check_fav = 0 ;
                if(Auth::check()) {

                    foreach ($favorits as $favorit) {

                        if($favorit->advertise_id == $advertise->id) {
                            $check_fav = 1 ;
                        }

                    }


                }

                if($check_fav == 0 ) {


                    $i_spam =   ' <i id="remover_fav'.$advertise->id.'" class="fa fa-bookmark-o" ></i> ';

                }else{

                    $i_spam =   ' <i id="remover_fav'.$advertise->id.'" class="fa fa-bookmark" ></i> ';

                }


                if($advertise->statustag_fory != null ) { $status_advertise = '<div class="ribbon-featured"><div class="ribbon-start"></div><div class="ribbon-content">آگهی ویژه</div><div class="ribbon-end"><figure class="ribbon-shadow"></figure></div></div>' ;   ; }else{$status_advertise = "" ;}
                if($advertise->statustag_vizheh == null ) { $color = "#fff" ; }else{$color = "#ff9595" ; }
                $job_name = \App\Job::find($advertise->job_id) ;
                $subjob_name = \App\SubJob::find($advertise->sub_job_id) ;



                $country_name = \App\Country::find($advertise->country_id) ;
                $state_name = \App\State::find($advertise->state_id) ;
                $city_name = \App\City::find($advertise->city_id) ;

                $v2 = verta( $advertise->created_at);
                $v2 = $v2->diffSeconds();

                if($v2 < 300 ) { $v2 = "دقایقی پیش" ;}
                elseif ($v2 > 300 && $v2 < 3600){
                    $v2 = intdiv((int)$v2, 60)   ;
                    $v2 = $v2." دقیه پیش "  ;
                }
                elseif ($v2 > 3600 && $v2 < 86400){
                    $v2 = intdiv((int)$v2, 3600)  ;
                    $v2 = $v2. " ساعت پیش "   ;
                }elseif( $v2 > 86400 && $v2 < 604800){

                    $v2 = intdiv((int)$v2, 86400)  ;
                    $v2 = $v2 . " روز پیش "   ;

                }else{

                    $v2 = intdiv((int)$v2, 604800)  ;
                    $v2 = $v2." هفته پیش "    ;

                }


                $artilces.='


                                                                     <li class="box-style-1 item col-lg-6 col-md-6 col-sm-12 col-xs-12 product type-product post-147 status-publish">

                                                                       '.$status_advertise.'
                                                                        <div class="wrapper">
                                                                            <span class="favorite" id="append_fav'.$advertise->id.'" >

                                                                            '.$i_spam.'

                                                                            </span>
                                                                            <span class="ad_visit">بازدید : '.$advertise->bazdid.'</span>
                                                                            <div style="height: 6rem;">

                                                                               <a href="/advertise/'.$advertise->slug.'/show_detail"><h3 style="padding-top: 28px !important;position: inherit;border-color: #cacaca;background: #f2f2f2;color: black">'.$advertise->title_ads.'</h3></a>

                                                                             </div>
                                                                             <a href="/advertise/'.$advertise->slug.'/show_detail">
                                                                            <div class="meta" style="background: '.$color.';">

                                                                                <div class="form-group" style="margin-top: 13px;">
                                                                                    <h5 style="line-height: 3rem;"><i style="margin-left: 5px;" class="fa fa-files-o"></i>'.$job_name->job_name.' ، '.$subjob_name->subjob_name.'</h5>
                                                                                    <h5 style="line-height: 3rem;"><i style="margin-left: 5px;" class="fa fa-map-marker  text-warning"></i>'.$country_name->country_name.' / '.$state_name->state_name.' / '.$city_name->city_name.'</h5>
                                                                                    <h5 style="line-height: 3rem;"><i style="margin-left: 5px;" class="fa fa-clock-o"></i>'.$v2.'</h5>
                                                                                </div>


                                                                            </div>
                                                                              </a>
                                                                            <!--end meta-->
                                                                            <!--end description-->
                                                                        </div>

                                                                    </li>


                                                              </a>


                                         ';
            }
            return $artilces;

        }


        return view('site.filter_ads'  );

    }








    public function filter_advertise()
    {

        return view('site.filter_advertise') ;
    }












    public function search(Request $request)
    {

        if($request->s != null || $request->s != "" ){

            Session::put('search', $request->s);
            $search = $request->s ;

        }





        if(Auth::check()) {
            $favorits = Favorite::where('user_id' , Auth::user()->id)->get() ;
        }


        $collection = collect([]);
        $citys = City::get() ;


        $advertises =  Advertise::query();
        $advertises->where('created_at', '<=', Carbon::now()->addMonths(1));
        $advertises->where('status' ,  2) ;
        $advertises->where('title_ads','LIKE', "%".Session::get('search')."%");







        if (Session::has('city') || Session::has('state')) {
            if (Session::has('city')) {

                $product_city = Session::get('city');

                foreach ($product_city as $key => $item) {

                    $collection->push((int)$item);

                }
            }

            if (Session::has('state')) {

                $state = Session::get('state');

                foreach ($state as $key => $item) {
                    foreach ($citys as $city) {

                        if ($city->state_id == $item) {
                            $collection->push($city->id);
                        }

                    }
                }
            }


            $advertises->whereIn('city_id', $collection);
        }


        $advertises = $advertises->orderBy('id' , 'desc')->paginate(10);





        $artilces = '';
        if ($request->ajax()) {



            foreach ($advertises as $advertise) {
                $check_fav = 0 ;
                if(Auth::check()) {

                    foreach ($favorits as $favorit) {

                        if($favorit->advertise_id == $advertise->id) {
                            $check_fav = 1 ;
                        }

                    }


                }

                if($check_fav == 0 ) {


                    $i_spam =   ' <i id="remover_fav'.$advertise->id.'" class="fa fa-bookmark-o" ></i> ';

                }else{

                    $i_spam =   ' <i id="remover_fav'.$advertise->id.'" class="fa fa-bookmark" ></i> ';

                }


                if($advertise->statustag_fory != null ) { $status_advertise = '<div class="ribbon-featured"><div class="ribbon-start"></div><div class="ribbon-content">آگهی ویژه</div><div class="ribbon-end"><figure class="ribbon-shadow"></figure></div></div>' ;   ; }else{$status_advertise = "" ;}
                if($advertise->statustag_vizheh == null ) { $color = "#fff" ; }else{$color = "#ff9595" ; }
                $job_name = \App\Job::find($advertise->job_id) ;
                $subjob_name = \App\SubJob::find($advertise->sub_job_id) ;



                $country_name = \App\Country::find($advertise->country_id) ;
                $state_name = \App\State::find($advertise->state_id) ;
                $city_name = \App\City::find($advertise->city_id) ;

                $v2 = verta( $advertise->created_at);
                $v2 = $v2->diffSeconds();

                if($v2 < 300 ) { $v2 = "دقایقی پیش" ;}
                elseif ($v2 > 300 && $v2 < 3600){
                    $v2 = intdiv((int)$v2, 60)   ;
                    $v2 = $v2." دقیه پیش "  ;
                }
                elseif ($v2 > 3600 && $v2 < 86400){
                    $v2 = intdiv((int)$v2, 3600)  ;
                    $v2 = $v2. " ساعت پیش "   ;
                }elseif( $v2 > 86400 && $v2 < 604800){

                    $v2 = intdiv((int)$v2, 86400)  ;
                    $v2 = $v2 . " روز پیش "   ;

                }else{

                    $v2 = intdiv((int)$v2, 604800)  ;
                    $v2 = $v2." هفته پیش "    ;

                }


                $artilces.='


                                                                     <li class="box-style-1 item col-lg-6 col-md-6 col-sm-12 col-xs-12 product type-product post-147 status-publish">

                                                                       '.$status_advertise.'
                                                                        <div class="wrapper">
                                                                            <span class="favorite" id="append_fav'.$advertise->id.'" >

                                                                            '.$i_spam.'

                                                                            </span>
                                                                            <span class="ad_visit">بازدید : '.$advertise->bazdid.'</span>
                                                                            <div style="height: 6rem;">

                                                                               <a href="/advertise/'.$advertise->slug.'/show_detail"><h3 style="padding-top: 28px !important;position: inherit;border-color: #cacaca;background: #f2f2f2;color: black">'.$advertise->title_ads.'</h3></a>

                                                                             </div>
                                                                             <a href="/advertise/'.$advertise->slug.'/show_detail">
                                                                            <div class="meta" style="background: '.$color.';">

                                                                                <div class="form-group" style="margin-top: 13px;">
                                                                                    <h5 style="line-height: 3rem;"><i style="margin-left: 5px;" class="fa fa-files-o"></i>'.$job_name->job_name.' ، '.$subjob_name->subjob_name.'</h5>
                                                                                    <h5 style="line-height: 3rem;"><i style="margin-left: 5px;" class="fa fa-map-marker  text-warning"></i>'.$country_name->country_name.' / '.$state_name->state_name.' / '.$city_name->city_name.'</h5>
                                                                                    <h5 style="line-height: 3rem;"><i style="margin-left: 5px;" class="fa fa-clock-o"></i>'.$v2.'</h5>
                                                                                </div>


                                                                            </div>
                                                                              </a>
                                                                            <!--end meta-->
                                                                            <!--end description-->
                                                                        </div>

                                                                    </li>


                                                              </a>


                                         ';
            }
            return $artilces;

        }


        return view('site.search' ,compact( 'search') );

    }


   public function ajax_delete_advertise(Request $request)
    {

         Advertise::destroy($request->id);

        return response()->json(['success'=> 'تغییر وضعیت با موفقیت انجام شد' ]);

    }


    public function ajax_send_takhalof(Advertise $advertise)
    {


        return response()->json(['success'=> 'با موفقیت ارسال شد' ]);
    }


    public function show_detail(Advertise $advertise)
    {


        $advertise->increment('bazdid');
        return view('site.advertise_show_details' , compact('advertise'));


    }
}
