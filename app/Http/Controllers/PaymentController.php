<?php

namespace App\Http\Controllers;

use App\Advertise;
use App\Payment;
use App\Price;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $payments = Payment::where('user_id' , Auth::user()->id)->where('status' , 0 )->paginate(30) ;
        return view('site.list_payment' , compact('payments')) ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


       $price = Price::find(1) ;
       $price = (int)$price->price_cv * (int)$request->cv_shop ;



            $payment = new Payment();
            $payment->user_id = Auth::user()->id ;
            $payment->price = $price ;
            $payment->tedad = (int)$request->cv_shop ;
            $payment->description = "افزایش اعتبار رزومه" ;
            $payment->save() ;


        $params = array(
            'order_id' => ''.$payment->id.'' ,
            'amount' => (int)$price ,
            'phone' => ''.Auth::user()->phone.'',
            'name' => ''.Auth::user()->name.'' ,
            'desc' => 'پرداخت برای افزایش اعتبار رزومه به شماره : '.'-'.$payment->id ,
            'callback' => 'http://kaarbin.ir/api/v1/callback_acount_cv',
        );

        $header = array(
            'Content-Type: application/json',
            'X-API-KEY: ff48a6cd-6b9a-49b7-a966-4eb88e07e22f',
            'X-SANDBOX: 1',
        );


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.idpay.ir/v1.1/payment');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $result = curl_exec($ch);
        curl_close($ch);

        $result = json_decode($result);

        if (empty($result) ||
            empty($result->link)) {

            print 'Error handeling';
            return FALSE;
        }


        return redirect()->to($result->link)->send();




    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function show(Payment $payment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function edit(Payment $payment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $price = Price::first() ;
        $total_price = 0 ;

        if($request->price_ads != null) {$total_price = (int)$total_price +  (int)$price->price_ads ;  }
        if($request->price_fory != null) {$total_price = (int)$total_price +  (int)$price->price_fory ;}
        if($request->price_vizheh != null) {$total_price = (int)$total_price +  (int)$price->price_vizheh ;}
        if($request->price_top != null) {$total_price = (int)$total_price +  (int)$price->price_top ;}
        if($request->price_nardeban != null) {$total_price = (int)$total_price +  (int)$price->price_nardeban ;}
        if($request->price_fory_nardeban != null) {$total_price = (int)$total_price +  (int)$price->price_fory_nardeban ;}
        if($request->price_tamdid_nardeban != null) {$total_price = (int)$total_price +  (int)$price->price_tamdid_nardeban ;}




            $payment = new Payment();
            $payment->user_id = Auth::user()->id;
            $payment->advertise_id = $id;
            $payment->price_ads = $request->price_ads;
            $payment->price_fory = $request->price_fory;
            $payment->price_vizheh = $request->price_vizheh;
            $payment->price_top = $request->price_top;
            $payment->price_nardeban = $request->price_nardeban;
            $payment->price_fory_nardeban = $request->price_fory_nardeban;
            $payment->price_tamdid_nardeban = $request->price_tamdid_nardeban;
            $payment->price = $total_price;
            $payment->save() ;


            $price_dargah = (int)$total_price - (int)Auth::user()->credit_price ;

            if ($price_dargah < 0 ) {

                Payment::where('id' , $payment->id)->update(['status' => 1 , 'description' => "پرداخت برای آگهی از اعتبار" , 'price' => $price_dargah]) ;

                $price_dargah =  abs($price_dargah) ;
                User::where('id' , Auth::user()->id)->update(['credit_price' =>  (int)$price_dargah ]);


                Advertise::where('id' , $payment->advertise_id)->update(['status' => 1 , 'status_peyment' => 1 , 'price_payment' => $total_price ]) ;


                if($payment->price_fory != null ){Advertise::where('id' , $payment->advertise_id)->update(['statustag_fory' => 1 ]);}
                if($payment->price_vizheh != null ){Advertise::where('id' , $payment->advertise_id)->update(['statustag_vizheh' => 1 ]);}
                if($payment->price_top != null ){Advertise::where('id' , $payment->advertise_id)->update(['statustag_top' => 1 ]);}
                if($payment->price_nardeban != null ){Advertise::where('id' , $payment->advertise_id)->update(['statustag_nardeban' => 1  , 'updated_at' => Carbon::now() ]);}
                if($payment->price_fory_nardeban != null ){Advertise::where('id' , $payment->advertise_id)->update(['statustag_nardeban' => 1 ,  'statustag_fory' => 1 , 'updated_at' => Carbon::now() ]);}
                if($payment->price_tamdid_nardeban != null ){Advertise::where('id' , $payment->advertise_id)->update(['statustag_nardeban' => 1   , 'updated_at' => Carbon::now() ]);}

                return redirect('/profile') ;


            }else{

                Payment::where('id' , $payment->id)->update(['price' => $price_dargah]) ;

                $price_dargah = $price_dargah."0" ;

                $params = array(
                    'order_id' => ''.$payment->id.'' ,
                    'amount' => (int)$price_dargah ,
                    'phone' => ''.Auth::user()->phone.'',
                    'name' => ''.Auth::user()->name.'' ,
                    'desc' => 'پرداخت برای آگهی به شماره : '.'-'.$payment->id ,
                    'callback' => 'http://kaarbin.ir/api/v1/callback_advertise',
                );

                $header = array(
                    'Content-Type: application/json',
                    'X-API-KEY: ff48a6cd-6b9a-49b7-a966-4eb88e07e22f',
                    'X-SANDBOX: 1',
                );


                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, 'https://api.idpay.ir/v1.1/payment');
                curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                $result = curl_exec($ch);
                curl_close($ch);

                $result = json_decode($result);

                if (empty($result) ||
                    empty($result->link)) {

                    print 'Error handeling';
                    return FALSE;
                }


                return redirect()->to($result->link)->send();


            }





    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Payment  $payment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Payment $payment)
    {
        //
    }
}
