<?php

namespace App\Http\Controllers;

use App\City;
use App\State;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class StateController extends Controller
{


    public function checksession()
    {


        $product = Session::get('state');
          $product_city = Session::get('city');

        return $product_city ;


    }





        public function index()
    {

        $states = State::paginate(16) ;
        return view('panel.list_state' , compact('states')) ;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('panel.add_state') ;

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $request->validate([

            'state_name'=> 'required',
            'country'=> 'required'

        ]);


        $state = new State();
        $state->country_id = $request->country;
        $state->state_name = $request->state_name;
        $state->save();

        Session::flash('status','با موفقیت ثبت شد');
        return redirect()->back();


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\State  $State
     * @return \Illuminate\Http\Response
     */
    public function show(State $state)
    {

        return view('panel.edit_state' , compact('state')) ;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\State  $State
     * @return \Illuminate\Http\Response
     */
    public function edit(State $state)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\State  $State
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, State $state)
    {

        $request->validate([

            'state_name'=> 'required',
            'country'=> 'required'

        ]);


        State::where('id' , $state->id)->update(['state_name' => $request->state_name , 'country_id' => $request->country ]) ;
        Session::flash('status','با موفقیت ویرایش شد');
        return redirect('/state');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\State  $State
     * @return \Illuminate\Http\Response
     */
    public function destroy(State $state)
    {
        State::where('id' , '=' , $state->id)->delete();

        Session::flash('status', 'با موفقیت حذف شد');
        return redirect('/state');
    }





    public function ajax_save_or_remove_state_in_session(Request $request)
    {

        $check_exist = 0 ;

        $citys = City::where('state_id' , $request->id )->get();

        if (Session::has('city')) {

            $product_city = Session::get('city');
            foreach ($product_city as $key => $item) {
                foreach ($citys as $city) {
                    if ($item == $city->id) {
                        $product_city->pull($key);
                    }
                }

            }

            $empty = 0 ;
            foreach ($product_city as $key => $item) {
                $empty = 1 ;
            }

            if($empty == 0) {

                Session::forget('city');

            }else{

                Session::put('city', $product_city);

            }



        }

        if (!Session::has('state')){
            if($request->status == "1" ) {
                $product = collect([]);
                $product->push($request->id);
                Session::put('state', $product);
                return response()->json(['success' => 'اضافه']);
            }else{

                return response()->json(['success' => 'اضافه']);
            }
        }else{

            $product = Session::get('state');
            foreach ($product as $key => $item) {
                if ($item == $request->id ) {
                    $check_exist = 1 ;
                    $get_key = $key ;
                }
            }

            if($check_exist == 1) {
                if($request->status == "0" ) {
                $product->pull($get_key);


                    $empty = 0 ;
                    foreach ($product as $key => $item) {
                        $empty = 1 ;
                    }

                    if($empty == 0) {

                        Session::forget('state');

                    }else{

                        Session::put('state', $product);

                    }



                return response()->json(['success'=> 'حذف' ]);
                }else{

                    return response()->json(['success'=> 'حذف' ]);

                }
            }else{
                if($request->status == "1" ) {
                    $product->push($request->id);
                    Session::put('state', $product);
                    return response()->json(['success' => 'اضافه']);
                }else{

                    return response()->json(['success' => 'اضافه']);

                }
            }


        }





//  return  Session::get('state');
//        $collection = collect([10,2,3]);
//        $collection->push(1);
//        $collection->push(4);
//        $collection->push(6);
//
//
//        foreach ($collection as $key => $item) {
//            if ($item == 1 ) {
//                $collection->pull($key);
//            }
//        }
//       $check =  $collection->contains(1);
//       return  $collection ;

//        foreach ($collection as $key => $item) {
//            if ($item == 1 ) {
//               $collection->pull($key);
//            }
//        }
        // $check =  $collection->contains(1);




    }




    public function ajax_remove_state_session(Request $request)
    {

        $product_state = Session::get('state');
        foreach ($product_state as $key => $item) {

            if ($item == $request->id) {

                $product_state->pull($key);

            }

        }

        $empty = 0 ;
        foreach ($product_state as $key => $item) {
            $empty = 1 ;
        }

        if($empty == 0) {

            Session::forget('state');

        }else{

            Session::put('state', $product_state);

        }



        return response()->json(['success' => 'اضافه']);

    }







}
