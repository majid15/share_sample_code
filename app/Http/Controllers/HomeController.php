<?php

namespace App\Http\Controllers;

use App\Advertise;
use App\City;
use App\Cv;
use App\Favorite;
use App\Job;
use App\Payment;
use App\Price;
use App\Showcv;
use App\SubJob;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Hekmatinasser\Verta\Facades\Verta;
use Trez\RayganSms\Facades\RayganSms;


class HomeController extends Controller
{

    public function update_user(Request $request)
    {

        $request->validate([
            'name' => ['required', 'string', 'max:50' , 'min:11'],
            'family' => ['required', 'string', 'max:50' , 'min:11'],

        ]);


        User::where('id' , Auth::user()->id)->update(['name' =>  $request->name, 'family' => $request->family, 'email' => $request->email]);

        Session::flash('status','با موفقیت ویرایش شد');
        return redirect()>back() ;
    }

    public function increaseacount(Request $request)
    {

        $request->validate([

            'price' =>  'required'

        ]);


        if ($request->price == "on") {
            if ($request->insertprice == null) {

                Session::flash('warning','لطفا مبلغی را وارد کنید');
                return redirect()->back() ;


            }else{

                $price= str_replace('', ',', $request->insertprice);
                $price = preg_replace('/[^A-Za-z0-9\-]/', '', $price);

                if((int)$price < 1000) {

                    Session::flash('warning','مبلغ وارد شده میبایست بالای 1 هزار تومان باشد');
                    return redirect()->back() ;

                }else{


                    $price = $price  ;


                    $payment = new Payment();
                    $payment->user_id = Auth::user()->id ;
                    $payment->price = $price ;
                    $payment->description = "افزایش اعتبار" ;
                    $payment->save() ;


                    $params = array(
                        'order_id' => ''.$payment->id.'' ,
                        'amount' => (int)$price ,
                        'phone' => ''.Auth::user()->phone.'',
                        'name' => ''.Auth::user()->name.'' ,
                        'desc' => 'پرداخت برای اعتبار به شماره : '.'-'.Auth::user()->id ,
                        'callback' => 'http://kaarbin.ir/api/v1/increase_acount',
                    );

                    $header = array(
                        'Content-Type: application/json',
                        'X-API-KEY: ff48a6cd-6b9a-49b7-a966-4eb88e07e22f',
                        'X-SANDBOX: 1',
                    );


                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, 'https://api.idpay.ir/v1.1/payment');
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                    $result = curl_exec($ch);
                    curl_close($ch);

                    $result = json_decode($result);

                    if (empty($result) ||
                        empty($result->link)) {

                        print 'Error handeling';
                        return FALSE;
                    }


                    return redirect()->to($result->link)->send();


                }




            }



        }else {

            if($request->insertprice == "" || $request->insertprice == null ) {

                Session::flash('warning', 'میبایست مبلغی را وارد کنید');
                return redirect()->back();

            }

            $price = str_replace('', ',', $request->insertprice);
            $price = preg_replace('/[^A-Za-z0-9\-]/', '', $price);



            if ((int)$price < 1000) {

                Session::flash('warning', 'مبلغ وارد شده میبایست بالای 1 هزار تومان باشد');
                return redirect()->back();

            } else {



                $payment = new Payment();
                $payment->user_id = Auth::user()->id ;
                $payment->price = $price ;
                $payment->description = "افزایش اعتبار" ;
                $payment->save() ;


                $params = array(
                    'order_id' => '' .$payment->id. '',
                    'amount' => (int)$price,
                    'phone' => '' . Auth::user()->phone . '',
                    'name' => '' . Auth::user()->name . '',
                    'desc' => 'پرداخت برای افزایش اعتبار به شماره : ' . '-' . Auth::user()->id ,
                    'callback' => 'http://kaarbin.ir/api/v1/increase_acount',
                );

                $header = array(
                    'Content-Type: application/json',
                    'X-API-KEY: ff48a6cd-6b9a-49b7-a966-4eb88e07e22f',
                    'X-SANDBOX: 1',
                );


                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, 'https://api.idpay.ir/v1.1/payment');
                curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                $result = curl_exec($ch);
                curl_close($ch);

                $result = json_decode($result);

                if (empty($result) ||
                    empty($result->link)) {

                    print 'Error handeling';
                    return FALSE;
                }


                return redirect()->to($result->link)->send();


            }
        }



    }

    public function ajax_change_subjob(Request $request)
    {

        $subJobs = SubJob::where('job_id' , $request->value)->get() ;
        $option = "" ;
        $subJob_name = "" ;

        $i = 0 ;
        foreach ($subJobs as $subJob){

            if($i == 0 ) {

                $subJob_name = $subJob->subjob_name	 ;

            }
            $appender =   '<option class="remove_subjob" value="'.$subJob->id.'" >  '.$subJob->subjob_name.'  </option>'  ;
            $option = $option .  $appender ;



            $i ++ ;
        }


        return response()->json(['success'=> 'تغییر وضعیت با موفقیت انجام شد' , 'option' => $option , 'subJob_name' => $subJob_name  ]);



    }



    public function ajax_check_credit(Request $request)
    {

       $check_show_cv  =  Showcv::where('user_id' , Auth::user()->id)->where('cv_id' , $request->id )->whereBetween('created_at', [Carbon::now()->subMonths(1), Carbon::now()])->get()->count() ;

       if($check_show_cv > 0 ) {

            $cv_id = Cv::find($request->id) ;
            $user = User::find($cv_id ->user_id) ;
            return response()->json(['success'=> 'show' ,   'phone' => $user->phone , 'email' => $user->email]);

       }

       $credit = Auth::user()->credit_cv ;

       $price = Price::find(1) ;

        $price_dargah = (int)$price->price_cv - (int)Auth::user()->credit_price ;

        if ($price_dargah < 0   || $credit > 0) { return response()->json(['success'=> 'ok' ]); } else{ return response()->json(['success'=> 'not_credit' ]); }



    }



    public function ajax_decrease_credit_cv(Request $request)
    {

        $show_cv = new Showcv() ;
        $show_cv->user_id = Auth::user()->id ;
        $show_cv->cv_id = $request->id ;
        $show_cv->job_id = $request->job_id ;
        $show_cv->save() ;

        $user = User::find(Auth::user()->id);

        if(Auth::user()->credit_cv > 0) {

            $user->decrement('credit_cv');

        }else{

            $price = Price::find(1) ;
            $new_credit =  (int)Auth::user()->credit_price - (int)$price->price_cv ;
            User::where('id' , Auth::user()->id)->update(['credit_price' => $new_credit]) ;

        }

        return response()->json(['success'=> 'ok' , 'phone' => $user->phone , 'email' => $user->email , 'credit' => $user->credit_cv ]);

    }



    public function pakbeshe1(Request $request)
    {

        foreach ($request->majid as $majid) {

            return $majid ;
        }

    }
    public function test()
    {
        //Auth::logout();
        return view('site.cv');
    }

    public function pakbeshe()
    {
        //Auth::logout();
        return view('site.pakbeshe');
    }

    public function index()
    {
        return view('home');
    }

    public function get_number(Request $request)
    {
        $request->validate([
            'phone' => ['required', 'string', 'max:11' , 'min:11'],

        ]);



        $code = rand(1001,9999) ;
        session(['code'=> $code]);
        session(['phone'=> $request->phone]);
        // Smsirlaravel::ultraFastSend(['password'=>$code],39595, $request->phone);
        RayganSms::sendAuthCode($request->phone, 'کد یکبار مصرف: '.$code.'', false);

        return redirect('/get_code');
    }


    public function save_name_family(Request $request)
    {

        $request->validate([
            'name' =>  'required' ,
            'family' =>  'required' ,

        ]);

        if ($request->code != null) {

            $User_moaref =  User::where('identifiercode' , $request->code)->first();
            $User_moaref_count =    User::where('identifiercode' , $request->code)->get()->count() ;

            if ($User_moaref_count != 0 ) {

                User::where('id' , Auth::user()->id)->update(['moaref_id' => $User_moaref->id , 'name' => $request->name, 'family' => $request->family]) ;
                Session::flash('status','نام و نام خانوادگی با موفقیت ثبت شد و معرف ثبت گردید');
                return redirect('/');
            }else{

                Session::flash('warning','کد معرف اشتباه یا به درستی وارد نشده بود');

            }



        }
        Session::flash('status','نام و نام خانوادگی با موفقیت ثبت شد');
        User::where('id' , Auth::user()->id)->update(['name' => $request->name, 'family' => $request->family]) ;
        $cv = new Cv() ;
        $cv->user_id = Auth::user()->id ;
        $cv->save() ;

        return redirect('/');

    }
    public function get_code(Request $request)
    {
        $code_session =  Session::get('code') ;
        return Validator::make($request, [
            'code' => 'required|in:'.$code_session,

        ],
            [
                'code.in' => 'کد وارد شده صحیح نیست',

            ]);



        $code = rand(10001,99999) ;
        session(['code'=> $code]);
        session(['phone'=> $request->phone]);
        // Smsirlaravel::ultraFastSend(['password'=>$code],39595, $request->phone);

        return redirect('/get_code');
    }


    public function filter_city($country , $state)
    {

        $citys = City::where('country_id' , $country)->where('state_id' , $state)->get() ;



//        $querys = City::query() ;
//        $querys->Orwhere('state_id' , 1) ;
//        $querys->Orwhere('country_id' , 2) ;
//        return $querys->get() ;

        return view('site.filter_city' , compact('citys' , 'state')) ;

    }

    public function filter(Request $request)
    {

        return view('site.filter') ;

    }


    public function listuser()
    {


        $users =   User::paginate(10) ;
        return view('panel.listuser' , compact('users' )   );


    }



    public function update(Request $request)
    {
        $request->validate([
            'level' => 'required',
            'id' => 'required',
        ]);

        User::where('id' , '=' , $request->id)->update(['admin'=> $request->level ]);

        $users =   User::paginate(10) ;
        return view('panel.listuser' , compact('users' )   );


    }


    public function first(Request $request)
    {

        $model = User::first() ;
        visitor()->visit($model);

        if(Auth::check()) {
            $favorits = Favorite::where('user_id' , Auth::user()->id)->get() ;
        }


        $collection = collect([]);
        $citys = City::get() ;


        $advertises =  Advertise::query();
        $advertises->whereBetween('created_at', [Carbon::now()->subMonths(1), Carbon::now()]) ;
        $advertises->where('status' ,  2) ;



        if (Session::has('city') || Session::has('state')) {
            if (Session::has('city')) {

                $product_city = Session::get('city');

                foreach ($product_city as $key => $item) {

                    $collection->push((int)$item);

                }
            }

            if (Session::has('state')) {

                $state = Session::get('state');

                foreach ($state as $key => $item) {
                    foreach ($citys as $city) {

                        if ($city->state_id == $item) {
                            $collection->push($city->id);
                        }

                    }
                }
            }


            $advertises->whereIn('city_id', $collection);
        }


        $advertises = $advertises->orderBy('id', 'desc')->paginate(10);


        $advertise_tops = Advertise::where('statustag_top' ,"!=", null)->where('created_at', '<=', Carbon::now()->addMonths(1))->get() ;


        $artilces = '';
        if ($request->ajax()) {



            foreach ($advertises as $advertise) {
                $check_fav = 0 ;
                if(Auth::check()) {

                    foreach ($favorits as $favorit) {

                        if($favorit->advertise_id == $advertise->id) {
                            $check_fav = 1 ;
                        }

                    }


                }

                if($check_fav == 0 ) {


                    $i_spam =   ' <i id="remover_fav'.$advertise->id.'" class="fa fa-bookmark-o" ></i> ';

                }else{

                    $i_spam =   ' <i id="remover_fav'.$advertise->id.'" class="fa fa-bookmark" ></i> ';

                }


                if($advertise->statustag_fory != null ) { $status_advertise = '<div class="ribbon-featured"><div class="ribbon-start"></div><div class="ribbon-content">آگهی ویژه</div><div class="ribbon-end"><figure class="ribbon-shadow"></figure></div></div>' ;   ; }else{$status_advertise = "" ;}
                if($advertise->statustag_vizheh == null ) { $color = "#fff" ; }else{$color = "#ff9595" ; }
                $job_name = \App\Job::find($advertise->job_id) ;
                $subjob_name = \App\SubJob::find($advertise->sub_job_id) ;



                $country_name = \App\Country::find($advertise->country_id) ;
                $state_name = \App\State::find($advertise->state_id) ;
                $city_name = \App\City::find($advertise->city_id) ;

                $v2 = verta( $advertise->created_at);
                $v2 = $v2->diffSeconds();

                if($v2 < 300 ) { $v2 = "دقایقی پیش" ;}
                elseif ($v2 > 300 && $v2 < 3600){
                    $v2 = intdiv((int)$v2, 60)   ;
                    $v2 = $v2." دقیه پیش "  ;
                }
                elseif ($v2 > 3600 && $v2 < 86400){
                    $v2 = intdiv((int)$v2, 3600)  ;
                    $v2 = $v2. " ساعت پیش "   ;
                }elseif( $v2 > 86400 && $v2 < 604800){

                    $v2 = intdiv((int)$v2, 86400)  ;
                    $v2 = $v2 . " روز پیش "   ;

                }else{

                    $v2 = intdiv((int)$v2, 604800)  ;
                    $v2 = $v2." هفته پیش "    ;

                }


                $artilces.='


                                                                     <li class="box-style-1 item col-lg-6 col-md-6 col-sm-12 col-xs-12 product type-product post-147 status-publish">

                                                                       '.$status_advertise.'
                                                                        <div class="wrapper">
                                                                            <span class="favorite" id="append_fav'.$advertise->id.'" >

                                                                            '.$i_spam.'

                                                                            </span>
                                                                            <span class="ad_visit">بازدید : '.$advertise->bazdid.'</span>
                                                                            <div style="height: 6rem;">

                                                                               <a href="/advertise/'.$advertise->slug.'/show_detail"><h3 style="padding-top: 28px !important;position: inherit;border-color: #cacaca;background: #f2f2f2;color: black">'.$advertise->title_ads.'</h3></a>

                                                                             </div>
                                                                             <a href="/advertise/'.$advertise->slug.'/show_detail">
                                                                            <div class="meta" style="background: '.$color.';">

                                                                                <div class="form-group" style="margin-top: 13px;">
                                                                                    <h5 style="line-height: 3rem;"><i style="margin-left: 5px;" class="fa fa-files-o"></i>'.$job_name->job_name.' ، '.$subjob_name->subjob_name.'</h5>
                                                                                    <h5 style="line-height: 3rem;"><i style="margin-left: 5px;" class="fa fa-map-marker  text-warning"></i>'.$country_name->country_name.' / '.$state_name->state_name.' / '.$city_name->city_name.'</h5>
                                                                                    <h5 style="line-height: 3rem;"><i style="margin-left: 5px;" class="fa fa-clock-o"></i>'.$v2.'</h5>
                                                                                </div>


                                                                            </div>
                                                                              </a>
                                                                            <!--end meta-->
                                                                            <!--end description-->
                                                                        </div>

                                                                    </li>


                                                              </a>


                                         ';
            }
            return $artilces;
        }

        return view('site.first' ,compact('advertise_tops') );

    }

    public function sendmessage()
    {

        $code = rand(1001,9999) ;
        session(['code'=> $code]);
        $phone =  Session::get('phone') ;
        RayganSms::sendAuthCode($phone, 'کد یکبار مصرف: '.$code.'', false);

        return response()->json(['success'=> "ok" ]);

    }


    public function destroy($id)
    {

        User::where('id' , '=' , $id)->delete();


        Session::flash('status', 'با موفقیت حذف شد');
        $users =   User::paginate(10) ;
        return view('panel.listuser' , compact('users' )   );


    }








    public function dashboard()
    {

// chart
        $a=array();
        $b=array();

        Verta::setStringformat('Y/n/j');

        for ($x = 0; $x <= 11; $x++) {


            $v = verta() ;

            $v->startYear();

            $start = $v->addMonths($x) ;

            $end  = $v->addMonths($x+1);


            $numbers1 = explode("/", $start);

            $sal = (string)$numbers1[0];
            $mah= (string)$numbers1[1];
            $roz = (string)$numbers1[2];
            $tarikh_shoro = Verta::getGregorian($sal,$mah,$roz) ;
            $tarikh_shoro  =    $tarikh_shoro [0]."-".$tarikh_shoro [1]."-".$tarikh_shoro [2] ;



            $numbers2 = explode("/", $end);

            $sal1 = (string)$numbers2[0];
            $mah1= (string)$numbers2[1];
            $roz1 = (string)$numbers2[2];
            $tarikh_payan = Verta::getGregorian($sal1,$mah1,$roz1) ;
            $tarikh_payan  =    $tarikh_payan [0]."-".$tarikh_payan [1]."-".$tarikh_payan [2] ;


            $sum = User::whereDate('created_at', '>=' , $tarikh_shoro )
                ->whereDate('created_at', '<' , $tarikh_payan )
                ->sum('credit_price') ;

            array_push($a,[$x+1,(int)$sum]);

        }

        $a = json_encode($a) ;




// pay month

        $v = verta() ;
        $start = $v->startMonth();
        $v = verta() ;
        $end = $v->endMonth();




        $numbers1 = explode("/", $start);

        $sal = (string)$numbers1[0];
        $mah= (string)$numbers1[1];
        $roz = (string)$numbers1[2];
        $tarikh_shoro = Verta::getGregorian($sal,$mah,$roz) ;
        $tarikh_shoro  =    $tarikh_shoro [0]."-".$tarikh_shoro [1]."-".$tarikh_shoro [2] ;



        $numbers2 = explode("/", $end);

        $sal1 = (string)$numbers2[0];
        $mah1= (string)$numbers2[1];
        $roz1 = (string)$numbers2[2];
        $tarikh_payan = Verta::getGregorian($sal1,$mah1,$roz1) ;
        $tarikh_payan  =    $tarikh_payan [0]."-".$tarikh_payan [1]."-".$tarikh_payan [2] ;


        $sum1 = User::whereDate('created_at', '>=' , $tarikh_shoro   )
            ->whereDate('created_at', '<=' , $tarikh_payan )
            ->sum('credit_price') ;






        // user count



        $count_user = User::get()->count() ;

        $count_user_month = User::whereDate('created_at', '>=' , $tarikh_shoro   )
            ->whereDate('created_at', '<=' , $tarikh_payan )
            ->get()
            ->count() ;


        // 	pay full

        $sum3 = User::sum('credit_price') ;

// 3 user

        $users = User::orderBy('id', 'DESC')->take(3)->get();

// 30 days month

        $v = verta() ;
        $start = $v->startMonth();
        $v = verta() ;
        $end = $v->endMonth();

        $numbers2 = explode("/", $end);
        $roz1 = (string)$numbers2[2];


        for ($x = 0; $x <= (int)$roz1-1 ; $x++) {

            $v = verta() ;
            $v->startMonth();
            $start = $v->addDays($x);

            $numbers1 = explode("/", $start);

            $sal = (string)$numbers1[0];
            $mah= (string)$numbers1[1];
            $roz = (string)$numbers1[2];
            $tarikh_shoro = Verta::getGregorian($sal,$mah,$roz) ;
            $tarikh_shoro  =    $tarikh_shoro [0]."-".$tarikh_shoro [1]."-".$tarikh_shoro [2] ;


            $sum2 = User::whereDate('created_at', '=' , $tarikh_shoro )
                ->sum('credit_price') ;


            array_push($b,[$x+1,(int)$sum2]);


        }


        $b = json_encode($b) ;



        $sum4 = ( (int)$sum3 * 1)/100 ;
        $sum4 = (int)$sum3 - (int)$sum4 ;


        return view('panel.dashboard', compact('a' , 'sum1' , 'count_user' , 'count_user_month' ,'sum3' , 'users' , 'b' , 'sum4' ));




    }













}
