<?php

namespace App\Http\Controllers;

use App\City;
use App\Citywant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class CitywantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */



    public function ajax_remove_citywant(Request $request)
    {

        Citywant::where('id' , $request->id)->delete() ;


        return response()->json(['success'=> 'تغییر وضعیت با موفقیت انجام شد' ]);


    }




    public function index()
    {

        $citywants = Citywant::paginate(16) ;
        return view('site.list_citywant' , compact('citywants')) ;

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return view('site.city_want'  ) ;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'country1' =>  'required' ,
            'state1' =>  'required' ,
            'city1' =>  'required' ,

        ]);


        $citywant = new Citywant();
        $citywant->user_id = Auth::user()->id ;
        $citywant->country_id = $request->country1 ;
        $citywant->state_id = $request->state1 ;
        $citywant->city_id = $request->city1 ;
        $citywant->save() ;

        Session::flash('status','با موفقیت ثبت شد');
        return redirect()->back() ;

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Citywant  $citywant
     * @return \Illuminate\Http\Response
     */
    public function show(Citywant $citywant)
    {

        return view('site.edit_city_want' , compact('citywant')) ;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Citywant  $citywant
     * @return \Illuminate\Http\Response
     */
    public function edit(Citywant $citywant)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Citywant  $citywant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Citywant $citywant)
    {

        $request->validate([
            'country' =>  'required' ,
            'state' =>  'required' ,
            'city' =>  'required' ,

        ]);

        Citywant::where('id' , $citywant->id)->update(['country_id' => $request->country ,'state' => $request->state ,   'city' => $request->city ]) ;

        Session::flash('status','با موفقیت ویرایش شد');
        return redirect()->back() ;

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Citywant  $citywant
     * @return \Illuminate\Http\Response
     */
    public function destroy(Citywant $citywant)
    {


    }
}
