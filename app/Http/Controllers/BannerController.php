<?php

namespace App\Http\Controllers;

use App\Banner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Intervention\Image\Facades\Image;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banners = Banner::get() ;
        return view('panel.listbanner' , compact('banners')) ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('panel.addbanner') ;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $request->validate([

            'image' => 'required',
            'title' => 'required',
            'url' => 'required',
        ]);



        if($request->hasFile('image')) {
            $image = request()->file('image');
            $imageName = time() . '-' . rand(10000, 99999) . "." . $image->extension();
            $imageLocation = public_path('/images/' . $imageName);
            $imageLocation1 = '/images/' . $imageName;
            Image::make($image)->resize(1920, 660)->save($imageLocation);
        }


        $banner = new Banner();
        $banner->title = $request->title ;
        $banner->url = $request->url  ;
        $banner->image = $imageLocation1 ;
        $banner->save();



        Session::flash('status', 'با موفقیت ذخیره شد');
        return redirect()->back();

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function show(Banner $banner)
    {

        return view('panel.editbanner' , compact('banner')) ;

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function edit(Banner $banner)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Banner $banner)
    {

        $request->validate([

            'image' => 'required',
            'title' => 'required',
            'url' => 'required',
        ]);



    if($request->hasFile('image')) {
        $image = request()->file('image');
        $imageName = time() . '-'. rand(10000, 99999).".". $image->extension();
        $imageLocation = public_path('/images/' . $imageName);
        $imageLocation1 = '/images/' . $imageName ;
        Image::make($image)->resize(1920,660)->save($imageLocation);

        Banner::where('id' , $banner->id)->update(['image' => $imageLocation1 ]) ;

    }


        Banner::where('id' , $banner->id)->update(['title' => $request->title , 'url' => $request->url  ]) ;


        Session::flash('status', 'با موفقیت ویرایش شد');
        return redirect()->back();


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function destroy(Banner $banner)
    {
        //
    }
}
