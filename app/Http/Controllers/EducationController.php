<?php

namespace App\Http\Controllers;

use App\Education;
use App\Historywork;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class EducationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function ajax_remove_education(Request $request)
    {

        Education::where('id' , $request->id)->delete() ;


        return response()->json(['success'=> 'تغییر وضعیت با موفقیت انجام شد' ]);


    }



    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('site.education'  ) ;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([

            'madrak' =>  'required' ,
            'reshteh' =>  'required' ,
            'daneshgah' =>  'required' ,

        ]);


        if($request->its_work == "on"){

            $year = null  ;
            $month = null;

        }else{

            $year = $request->year_end   ;
            $month = $request->month_end ;

        }


        $education = new Education();
        $education->user_id = Auth::user()->id ;
        $education->madrak = $request->madrak ;
        $education->reshteh = $request->reshteh ;
        $education->daneshgah = $request->daneshgah ;
        $education->year = $year ;
        $education->month = $month ;
        $education->description_edu = $request->description ;

        $education->save() ;

        Session::flash('status','با موفقیت ثبت شد');
        return redirect()->back() ;



    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Education  $education
     * @return \Illuminate\Http\Response
     */
    public function show(Education $education)
    {
        return view('site.edit_education' , compact('education'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Education  $education
     * @return \Illuminate\Http\Response
     */
    public function edit(Education $education)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Education  $education
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Education $education)
    {


        $request->validate([

            'madrak' =>  'required' ,
            'reshteh' =>  'required' ,
            'daneshgah' =>  'required' ,

        ]);


        if($request->its_work != null){

            $year = null  ;
            $month = null;

        }else{

            $year = $request->year_end   ;
            $month = $request->month_end ;

        }

        Education::where('id' , $education->id)->update(['madrak' => $request->madrak
            , 'reshteh' => $request->reshteh
            , 'daneshgah' => $request->daneshgah
            , 'year' => $year
            , 'month' => $month
            ,'description_edu' => $request->description   ]) ;


        Session::flash('status','با موفقیت اپدیت شد');
        return redirect()->back() ;





    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Education  $education
     * @return \Illuminate\Http\Response
     */
    public function destroy(Education $education)
    {
        //
    }
}
