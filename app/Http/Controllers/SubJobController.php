<?php

namespace App\Http\Controllers;

use App\Job;
use App\SubJob;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SubJobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $subjobs = SubJob::paginate(16) ;


        return view('panel.list_subjob' , compact('subjobs')) ;


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {


        return view('panel.add_subjob' ) ;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([

            'subjob_name'=> 'required',
            'job'=> 'required',

        ]);


        $subjob = new SubJob();
        $subjob->job_id = $request->job ;
        $subjob->subjob_name = $request->subjob_name ;
        $subjob->save() ;

        Session::flash('status','با موفقیت ثبت شد');
        return redirect()->back();


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Daneshkadeh  $daneshkadeh
     * @return \Illuminate\Http\Response
     */
    public function show(SubJob $subjob)
    {

        return view('panel.edit_subjob' , compact('subjob')) ;


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Daneshkadeh  $daneshkadeh
     * @return \Illuminate\Http\Response
     */
    public function edit(SubJob $subjob)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Daneshkadeh  $daneshkadeh
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SubJob $subjob)
    {
        $request->validate([

            'subjob_name'=> 'required',
            'job'=> 'required',

        ]);


        SubJob::where('id' , $subjob->id)->update(['subjob_name' => $request->subjob_name , 'job_id' => $request->job  ]) ;
        Session::flash('status','با موفقیت اپدیت شد');
        return redirect()->back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Daneshkadeh  $daneshkadeh
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubJob $subJob)
    {
        SubJob::where('id' , '=' , $subJob->id)->delete();

        Session::flash('status', 'با موفقیت حذف شد');
        return redirect()->back() ;
    }
}
