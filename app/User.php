<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Shetabit\Visitor\Traits\Visitable;
use Shetabit\Visitor\Traits\Visitor;

class User extends Authenticatable
{
    use Notifiable;
    use Visitor;
    use Visitable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */




    protected $fillable = [
        'name', 'email', 'password','admin','name','family','image','phone','identifiercode','api_token','credit_price','credit_cv'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function Advertises()
    {
        return $this->hasMany(Advertise::class);
    }

    public function Cvs()
    {
        return $this->hasMany(Cv::class);
    }


    public function Favorites()
    {
        return $this->hasMany(Favorite::class);
    }


    public function Payments()
    {
        return $this->hasMany(Payment::class);
    }


    public function Showcvs()
    {
        return $this->hasMany(Showcv::class);
    }


    public function Tickets()
    {
        return $this->hasMany(Ticket::class);
    }


    public function Messages()
    {
        return $this->hasMany(Message::class);
    }


    public function Messagereads()
    {
        return $this->hasMany(Messageread::class);
    }

}
