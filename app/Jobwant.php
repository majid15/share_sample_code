<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jobwant extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
