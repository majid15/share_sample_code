<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticketdetail extends Model
{
    public function Ticket()
    {
        return $this->belongsTo(Ticket::class);
    }
}
