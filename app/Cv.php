<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Cv extends Model
{
    use Sluggable;

    public function user()
    {
        return $this->belongsTo(User::class);
    }



    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'url'
            ]
        ];
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }
}
