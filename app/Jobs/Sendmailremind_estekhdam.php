<?php

namespace App\Jobs;

use App\Mail\Sendmai_estekhdam;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Mail;

class Sendmailremind_estekhdam implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $one ;
    public $two ;
    public $three ;
    public $four ;

    public function __construct($one , $two , $three , $four )
    {

        $this->one = $one ;
        $this->two = $two ;
        $this->three = $three ;
        $this->four = $four ;

    }

    public function handle()
    {
        Mail::to("info@sa-sha.ir")->send(new Sendmai_estekhdam( $this->one , $this->two , $this->three , $this->four )) ;
    }
}
