<?php

namespace App;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Advertise extends Model
{
    use Sluggable;


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function Optionads()
    {
        return $this->hasMany(Optionads::class);
    }


    public function Jobs()
    {
        return $this->hasMany(Job::class);
    }


    public function Areas()
    {
        return $this->hasMany(Area::class);
    }


    public function Favorites()
    {
        return $this->hasMany(Favorite::class);
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'url'
            ]
        ];
    }


    public function getRouteKeyName()
    {
        return 'slug';
    }

}
