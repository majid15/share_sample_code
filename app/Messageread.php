<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Messageread extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }


    public function Message()
    {
        return $this->belongsTo(Message::class);
    }

}
