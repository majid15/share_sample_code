<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Sendmai extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $one ;
    public $two ;
    public $three ;
    public $four ;

    public function __construct($one , $two , $three , $four )
    {

        $this->one = $one ;
        $this->two = $two ;
        $this->three = $three ;
        $this->four = $four ;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        return $this->view('welcome')
            ->subject(' - ارسال از ساشا'. $this->two)  ;
    }
}
