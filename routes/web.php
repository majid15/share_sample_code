<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@first')->name('first');
Route::get('/search', 'AdvertiseController@search')->name('search');

Route::get('/filter_ads', 'AdvertiseController@filter_ads')->name('filter_ads');
Route::get('/filter_advertise', 'AdvertiseController@filter_advertise')->name('filter_advertise');

Route::get('/filter', 'HomeController@filter')->name('filter');
Route::get('/filter/{country}/{state}', 'HomeController@filter_city')->name('filter_city');
 
Route::get('/advertise/{advertise}/show_detail', 'AdvertiseController@show_detail')->name('show_detail');
Route::get('/checksession', 'StateController@checksession')->name('checksession');




/*
auth
*/
Route::get('/logout', function () {
    \Illuminate\Support\Facades\Auth::logout();
    return redirect('/get_number') ;
})->name('logout');

Route::get('/pakbeshe', 'HomeController@pakbeshe');

Route::get('/get_number', function () {
    return view('auth.get_number');
})->name('login_');

Route::post('/get_number', 'HomeController@get_number')->name('get_number');
Route::post('/save_name_family', 'HomeController@save_name_family')->name('save_name_family');

Route::get('/get_code', function () {
    return view('auth.getcode');
});

Route::post('/get_code', 'HomeController@get_code')->name('get_code');
Route::post('/login_', 'Auth\LoginController@login')->name('loginuser');


Route::get('/register_', function () {
    return view('auth.register');
});


/*
auth
*/


Route::group(['middleware' => ['auth']], function () {

    Route::post('/update_user', 'HomeController@update_user')->name('update_user');
    Route::post('/increaseacount', 'HomeController@increaseacount')->name('increaseacount');

    Route::get('/shop_cv', function () {
        return view('site.shop_cv');
    });

    Route::get('/advertise_edit/{advertise}', function (\App\Advertise $advertise) {
        return view('site.edit_advertise' , compact('advertise'));
    })->name('advertise_edit');

    Route::get('/profile', function () {
        return view('site.profile');
    });

    Route::get('/list_main', function () {
        return view('site.list_main');
    });

    Route::get('/favorite', 'FavoriteController@favorite')->name('favorite');

    Route::get('/increase_acount', function () {
        return view('site.increase_acunt');
    });

    Route::get('/users', 'HomeController@listuser')->name('listuser');
    Route::post('/edit_level_user', 'HomeController@update')->name('updateleveluser');
    Route::delete('/delete_user/{id}', 'HomeController@destroy')->name('deleteuser');

    Route::get('/dashboard', 'HomeController@dashboard')->name('dashboard');
    Route::get('/list_cv/{advertise}', 'CvController@list_cv')->name('list_cv');

    Route::resource('cv', 'CvController');
    Route::resource('advertise', 'AdvertiseController');
    Route::resource('job', 'JobController');
    Route::resource('subjob', 'SubJobController');
    Route::resource('country', 'CountryController');
    Route::resource('state', 'StateController');
    Route::resource('city', 'CityController');
    Route::resource('citywant', 'CitywantController');
    Route::resource('jobwant', 'JobwantController');
    Route::resource('historywork', 'HistoryworkController');
    Route::resource('education', 'EducationController');
    Route::resource('price', 'PriceController');
    Route::resource('payment', 'PaymentController');
    Route::resource('banner', 'BannerController');
    Route::resource('payamha', 'MessageController');
    Route::resource('/transaction', 'PaymentController') ;



    Route::post('/ajax_change_state', 'CountryController@ajax_change_state');
    Route::post('/ajax_change_city', 'CountryController@ajax_change_city');
    Route::post('/ajax_change_subjob', 'HomeController@ajax_change_subjob');
    Route::post('/ajax_remove_history', 'HistoryworkController@ajax_remove_history');
    Route::post('/ajax_remove_education', 'EducationController@ajax_remove_education');
    Route::post('/ajax_remove_jobwant', 'JobwantController@ajax_remove_jobwant');
    Route::post('/ajax_remove_citywant', 'CitywantController@ajax_remove_citywant');
    Route::post('/ajax_delete_advertise', 'AdvertiseController@ajax_delete_advertise');





    /*
    all ajax controller
    */
    
    Route::post('/ajax_save_or_remove_state_in_session', 'StateController@ajax_save_or_remove_state_in_session');
    Route::post('/ajax_save_or_remove_city_in_session', 'CityController@ajax_save_or_remove_city_in_session');
    Route::post('/ajax_remove_state_session', 'StateController@ajax_remove_state_session');
    Route::post('/ajax_remove_city_session', 'CityController@ajax_remove_city_session');
    Route::post('/ajax_send_takhalof', 'AdvertiseController@ajax_send_takhalof');
    Route::post('/ajax_read_message', 'MessageController@ajax_read_message');
    Route::post('/ajaxsendmessage', 'HomeController@sendmessage');
    Route::post('/ajax_add_to_favorite', 'FavoriteController@ajax_add_to_favorite');
    Route::post('/ajax_remove_to_favorite', 'FavoriteController@ajax_remove_to_favorite');
    Route::post('/ajax_check_credit', 'HomeController@ajax_check_credit');
    Route::post('/ajax_decrease_credit_cv', 'HomeController@ajax_decrease_credit_cv');

    /*
   all ajax controller
    */





    /*
   advertise panel
    */

    Route::get('/list_advertise_expire', function () {
        $advertises =  \App\Advertise::where('status' , 4)->paginate(16);
        return view('panel.list_advertise_expire' , compact('advertises'));
    });

    Route::get('/list_advertise_aprove', function () {
        $advertises =   \App\Advertise::where('status' , 2)->paginate(16);
        return view('panel.list_advertise_aprove', compact('advertises'));
    });

    Route::get('/list_advertise_wait_payment', function () {
        $advertises =   \App\Advertise::where('status' , 0)->paginate(16);
        return view('panel.list_advertise_wait', compact('advertises'));
    });

    Route::get('/list_advertise_wait', function () {
        $advertises =   \App\Advertise::where('status' , 1)->paginate(16);
        return view('panel.list_advertise_wait', compact('advertises'));
    });


    Route::get('/list_advertise_deslice', function () {
        $advertises =   \App\Advertise::where('status' , 3)->paginate(16);
        return view('panel.list_advertise_deslice', compact('advertises')) ;
    });

    Route::post('/updateleveladvertise', 'AdvertiseController@updateleveladvertise')->name('updateleveladvertise');


    /*
     /advertise panel
   */



    Route::get('/contact_us', function () {
        return view('site.contact_us');
    });

    Route::get('/about_us', function () {
        return view('site.about_us');
    });

    Route::get('/privacy', function () {
        return view('site.privacy');
    });



});
