<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->namespace('Api\v1')->group(function (){

    Route::post('/callback_increase_acount' , 'ApiController@callback_increase_acount') ;
    Route::post('/callback_advertise' , 'ApiController@callback_advertise') ;
    Route::post('/callback_acount_cv' , 'ApiController@callback_acount_cv') ;


});
