/* Theme Customize JS */

(function ($) {
    "use strict";
    // Create by Nguyen Duc Viet

    //cart dropdown
    $(document).on("mouseenter", ".topcart", function () {
        $(this).find(".topcart_content").stop().slideDown(500);
    }).on("mouseleave", ".topcart", function () {
        $(this).find(".topcart_content").stop().slideUp(500);
    });


    //Category view mode
    $(document).on("click", ".view-mode > a", function () {
        $(this).addClass("active").siblings(".active").removeClass("active");
        if ($(this).hasClass("grid")) {
            $(".shop-products").removeClass("list-view");
            $(".shop-products").addClass("grid-view");
            $(".list-col4").removeClass("col-xs-12 col-sm-4");
            $(".list-col8").removeClass("col-xs-12 col-sm-8");
        } else {
            $(".shop-products").addClass("list-view");
            $(".shop-products").removeClass("grid-view");
            $(".list-col4").addClass("col-xs-12 col-sm-4");
            $(".list-col8").addClass("col-xs-12 col-sm-8");
        }
    });

    //quickview button
    $(document).on("click", "a.quickview", function (event) {
        event.preventDefault();
        var productID = $(this).attr("data-quick-id");
        showQuickView(productID);
    });

    $(document).on("click", ".closeqv", function () {
        hideQuickView();
    });

    //toggle categories event
    $(document).on("click", "#secondary .product-categories .cat-parent .opener", function () {
        if ($(this).parent().hasClass("opening")) {
            $(this).parent().removeClass("opening").children("ul").stop().slideUp(300);
        } else {
            $(this).parent().siblings(".opening").removeClass("opening").children("ul").stop().slideUp(300);
            $(this).parent().addClass("opening").children("ul").stop().slideDown(300);
        }
    });

    // fix tab content load wow & slideshow
    $(document).on("click", ".vc_tta-tabs-list > li", function () {
        var currentP = $(window).scrollTop();
        $("body, html").animate({ 'scrollTop': currentP + 1 }, 10);
        var tab_bodyID = $(this).children("a").attr("href");
        if ($(tab_bodyID).length) {
            if ($(tab_bodyID).find(".extra-block .su-slider").length) {
                $(tab_bodyID).find(".extra-block .su-slider").each(function () {
                    // Prepare data
                    var $slider = $(this);
                    var $swiper = $slider.swiper({
                        wrapperClass: "su-slider-slides",
                        slideClass: "su-slider-slide",
                        slideActiveClass: "su-slider-slide-active",
                        slideVisibleClass: "su-slider-slide-visible",
                        pagination: "#" + $slider.attr("id") + " .su-slider-pagination",
                        autoplay: $slider.data("autoplay"),
                        paginationClickable: true,
                        grabCursor: true,
                        mode: "horizontal",
                        mousewheelControl: $slider.data("mousewheel"),
                        speed: $slider.data("speed"),
                        calculateHeight: $slider.hasClass("su-slider-responsive-yes"),
                        loop: true
                    });
                    // Prev button
                    $slider.find(".su-slider-prev").click(function (e) {
                        $swiper.swipeNext();
                    });
                    // Next button
                    $slider.find(".su-slider-next").click(function (e) {
                        $swiper.swipePrev();
                    });
                });
            }
        }
    });

    // categories menu mobile toggle
    $(document).on("click", ".categories-menu li.menu-item-has-children > .opener, .categories-menu li.page_item_has_children > .opener", function () {
        if ($(this).parent().hasClass("opening")) {
            $(this).parent().removeClass("opening").children("ul").stop().slideUp(300);
        } else {
            $(this).parent().siblings(".opening").removeClass("opening").children("ul").stop().slideUp(300);
            $(this).parent().addClass("opening").children("ul").stop().slideDown(300);
        }
    });

    $(document).on("click", ".toggle-top-bar", function () {
        $(this).parent().toggleClass("show");
        var parH = $(this).parent().outerHeight();
        if (!$(this).parent().hasClass("show")) {
            $(this).parent().css("top", (-parH) + "px");
        } else {
            $(this).parent().css("top", 0);
        }
    });

    //sidebar mobile toggle
    $(document).on("click", ".sidebar-toggle", function () {
        $(this).parent().toggleClass("opening");
        $(this).siblings().slideToggle(400);
    });

    // visual select list
    $(document).on("change", "select.vitual-style-el", function () {
        var my_val = $(this).children(":selected").text();
        $(this).parent().children(".vitual-style").text(my_val);
    });

    //toggle categories menu
    $(document).on("click", ".categories-menu .catmenu-opener", function () {
        if ($("#main-content").hasClass("home1-content") && $(window).scrollTop() == 0) {
            return false;
        } else {
            $(this).parent().toggleClass("opening");
        }
    });
    $(document).on("click", ".categories-menu .showmore-cats", function () {
        $(this).toggleClass("expanded");
        $(this).prev(".menu-list-wrapper").toggleClass("all");
        $(".categories-menu li.out-li").stop().slideToggle(300);
    });


})(jQuery);


jQuery(document).ready(function ($) {
    //loading when add to cart
    $("body").append('<div id="loading"></div>');
    $(document).ajaxComplete(function (event, request, options) {
        if (options.url.indexOf("wc-ajax=add_to_cart") != -1) {
            var title = jQuery("a.added_to_cart").attr("title");
            jQuery("a.added_to_cart").removeAttr("title").closest("p.add_to_cart_inline").attr("data-original-title", title);
            $("html, body").animate({ scrollTop: 0 }, 1000, function () {
                $(".topcart .topcart_content").stop().slideDown(500);
            });
        }
        $("#loading").fadeOut(400);
    });
    $(document).ajaxSend(function (event, xhr, options) {
        if (options.url.indexOf("wc-ajax=add_to_cart") != -1) {
            $("#loading").show();
        }
        if (options.url.indexOf("wc-ajax=get_refreshed_fragments") != -1) {
            if ($("body").hasClass("fragments_refreshed")) {
                xhr.abort();
            } else {
                $("body").addClass("fragments_refreshed");
            }
        }
    });


    //init for owl carousel
    var owl = $('[data-owl="slide"]');
    owl.each(function (index, el) {
        var $item = $(this).data("item-slide");
        var $rtl = $(this).data("ow-rtl");
        var $dots = ($(this).data("dots") == true) ? true : false;
        var $nav = ($(this).data("nav") == false) ? false : true;
        var $margin = ($(this).data("margin")) ? $(this).data("margin") : 0;
        var $desksmall_items = ($(this).data("desksmall")) ? $(this).data("desksmall") : (($item) ? $item : 4);
        var $tablet_items = ($(this).data("tablet")) ? $(this).data("tablet") : (($item) ? $item : 2);
        var $tabletsmall_items = ($(this).data("tabletsmall")) ? $(this).data("tabletsmall") : (($item) ? $item : 2);
        var $mobile_items = ($(this).data("mobile")) ? $(this).data("mobile") : (($item) ? $item : 1);
        var $tablet_margin = Math.floor($margin / 1.5);
        var $mobile_margin = Math.floor($margin / 3);
        var $default_items = ($item) ? $item : 5;
        $(this).owlCarousel({
            nav: $nav,
            dots: $dots,
            margin: $margin,
            rtl: true,
            items: $default_items,
            responsive: {
                0: {
                    items: $mobile_items, // In this configuration 1 is enabled from 0px up to 479px screen size 
                    margin: $mobile_margin
                },

                480: {
                    items: $tabletsmall_items, // from 480 to 639 default 1
                    margin: $tablet_margin
                },

                640: {
                    items: $tablet_items, // from this breakpoint 640 to 959 default 2
                    margin: $tablet_margin
                },

                960: {
                    items: $desksmall_items, // from this breakpoint 960 to 1199 default 3
                    margin: $margin

                },
                1200: {
                    items: $default_items,
                }
            },
            onTranslated: function (element, info, callbackName) {
                $(element.currentTarget).find(".owl-item").removeClass("first-act").removeClass("last-act");
                $(element.currentTarget).find(".owl-item.active").each(function (index) {
                    if (index == 0) {
                        $(this).addClass("first-act");
                    }
                    if (index == $(element.currentTarget).find(".owl-item.active").size() - 1) {
                        $(this).addClass("last-act");
                    }
                });
            }
        });
    });



    //Add quick view box
    $("body").append('<div class="quickview-wrapper"><div class="overlay-bg" style="position: absolute; width: 100%; height: 100%; top: 0; left: 0; z-index: 10; cursor: pointer;" onclick="hideQuickView()"></div><div class="quick-modal"><span class="qvloading"></span><span class="closeqv"><i class="fa fa-times"></i></span><div id="quickview-content"></div><div class="clearfix"></div></div></div>');

    // init Animate Scroll
    if ($("body").hasClass("hanoistore-animate-scroll") && !Modernizr.touch) {
        wow = new WOW(
			{
			    mobile: false,
			}
		);
        wow.init();
    }

    //Go to top
    $(document).on("click", "#back-top", function () {
        $("html, body").animate({ scrollTop: 0 }, "slow");
    });

    // Scroll
    var currentP = 0;

    $(window).scroll(function () {
        var headerH = $(".header-container").height();
        var scrollP = $(window).scrollTop();

        if ($(window).width() > 1024) {
            if (scrollP != currentP) {
                //Back to top
                if (scrollP >= headerH) {
                    $("#back-top").addClass("show");
                } else {
                    $("#back-top").removeClass("show");
                }

                currentP = $(window).scrollTop();
            }
        }
        if ($(".header-container > .nav-menus.sticky .categories-menu").hasClass("opening")) {
            $(".header-container > .nav-menus.sticky .categories-menu").removeClass("opening");
        }
    });

    //tooltip
    $(".yith-wcwl-share a, .social-icons a").each(function () {
        var text = $.trim($(this).text());
        var title = $.trim($(this).attr("title"));
        $(this).attr("data-toggle", "tooltip");
        if (!title) {
            $(this).attr("title", text);
        }
    });

    $('[data-toggle="tooltip"]').tooltip({ container: "body" });

    //sidebar mobile
    $("#archive-product, #main-column").each(function () {
        if ($(this).next("#secondary").length) {
            $(this).next("#secondary").addClass("right-sidebar").append('<span class="sidebar-toggle fa fa-list-alt"></span>');
        }
        if ($(this).prev("#secondary").length) {
            $(this).prev("#secondary").addClass("left-sidebar").append('<span class="sidebar-toggle fa fa-list-alt"></span>');
        }
    });
    //mobile menu display
    $(document).on("click", ".nav-mobile .toggle-menu, .mobile-menu-overlay", function () {
        $("body").toggleClass("mobile-nav-on");
    });
    $(".categories-menu ul.mega_main_menu_ul li.menu-item-has-children, .categories-menu li.page_item_has_children").append('<span class="opener fa fa-angle-right"></span>');
    $(".mobile-menu li.dropdown").append('<span class="toggle-submenu"><i class="fa fa-angle-right"></i></span>');
    $(document).on("click", ".mobile-menu li.dropdown .toggle-submenu", function () {
        if ($(this).parent().siblings(".opening").length) {
            var old_open = $(this).parent().siblings(".opening");
            old_open.children("ul").stop().slideUp(200);
            old_open.children(".toggle-submenu").children(".fa").removeClass("fa-angle-down").addClass("fa-angle-right");
            old_open.removeClass("opening");
        }
        if ($(this).parent().hasClass("opening")) {
            $(this).parent().removeClass("opening").children("ul").stop().slideUp(200);
            $(this).parent().children(".toggle-submenu").children(".fa").removeClass("fa-angle-down").addClass("fa-angle-right");
        } else {
            $(this).parent().addClass("opening").children("ul").stop().slideDown(200);
            $(this).parent().children(".toggle-submenu").children(".fa").removeClass("fa-angle-right").addClass("fa-angle-down");
        }
    });

    //gird layout auto arrange
    $(".auto-grid").each(function () {
        var $col = ($(this).data("col")) ? $(this).data("col") : 4;
        $(this).autoGrid({
            no_columns: $col
        });
    });

    //Fancy box for single project
    $(".prfancybox").fancybox({
        openEffect: "fade",
        closeEffect: "elastic",
        nextEffect: "fade",
        prevEffect: "fade",
        helpers: {
            title: {
                type: "inside"
            },
            overlay: {
                showEarly: false
            },
            buttons: {},
            thumbs: {
                width: 100,
                height: 100
            }
        }
    });
    $(".filter-options .btn").click(function () {
        $(this).siblings(".btn").removeClass("active");
        $(this).addClass("active");
        var filter = $(this).data("group");
        if (filter) {
            if (filter == "all") {
                $("#projects_list .project").removeClass("hide");
            } else {
                $("#projects_list .project").each(function () {
                    var my_group = $(this).data("groups");
                    if (my_group.indexOf(filter) != -1) {
                        $(this).removeClass("hide");
                    } else {
                        $(this).addClass("hide");
                    }
                });
            }
        }
        $(window).resize();
    });

    //project gallery
    jQuery(".project-gallery .sub-images").owlCarousel({
        items: 5,
        nav: true,
        dots: true,
        rtl: true,
        responsive: {
            0: {
                items: 3
            },

            480: {
                items: 3
            },

            640: {
                items: 4
            },

            991: {
                items: 5

            },
            1199: {
                items: 5
            }
        }
    });


    //select html vitual style
    $("select.vitual-style-el").each(function () {
        var my_val = $(this).children(":selected").text();
        if (!$(this).parent().hasClass("vitual-style-wrap")) {
            $(this).wrap('<div class="vitual-style-wrap"></div>');
        }
        if (!$(this).parent().children(".vitual-style").length) {
            $(this).parent().append('<span class="vitual-style">' + my_val + "</span>");
        } else {
            $(this).parent().children(".vitual-style").text(my_val);
        }
    });

    //categories menu
    if ($("#main-content").hasClass("home1-content")) {
        var winW = $(window).width();
        if (winW >= 1024) {
            $(".categories-menu").addClass("opening");
        }
    }

    // toggle product categories list
    $("#secondary .product-categories .cat-parent").append('<span class="opener fa fa-plus"></span>');

    //product countdown
    window.setInterval(function () {
        $(".deals-countdown").each(function () {
            var me = $(this);
            var days = parseInt(me.find(".days_left").text());
            var hours = parseInt(me.find(".hours_left").text());
            var mins = parseInt(me.find(".mins_left").text());
            var secs = parseInt(me.find(".secs_left").text());
            if (days > 0 && hours >= 0 && mins >= 0 && secs >= 0) {
                if (secs == 0) {
                    secs = 59;
                    if (mins == 0) {
                        mins = 59;
                        if (hours == 0) {
                            hours = 23;
                            if (days = 0) {
                                hours = 0;
                                mins = 0;
                                secs = 0;
                            } else {
                                days = days - 1;
                            }
                        } else {
                            hours = hours - 1;
                        }
                    } else {
                        mins = mins - 1;
                    }
                } else {
                    secs = secs - 1;
                }
                me.find(".days_left").html(days);
                me.find(".hours_left").html(hours);
                me.find(".min_left").html(mins);
                me.find(".secs_left").html(secs);
            }
        });
    }, 1000);

});//end of document ready
jQuery(window).resize(function () {
    //default topbar position
    jQuery(".header-container.layout1 .top-bar").css("top", (-jQuery(".header-container.layout1 .top-bar").outerHeight()) + "px");
});
jQuery(window).bind("load", function () {
    var el = (jQuery(".categories-menu .mega_main_menu_ul").length) ? ".mega_main_menu_ul" : ".nav_menu";
    var items = parseInt(jQuery(".categories-menu .showmore-cats").data("items"));
    var first_lv_items = jQuery(".categories-menu").find(el + " > li.menu-item").size();
    jQuery(".categories-menu").find(el + " > li.menu-item").each(function (index) {
        if (index > items - 1) {
            jQuery(this).addClass("out-li").hide();
        }
    });
    if (first_lv_items > items) {
        jQuery(".categories-menu .showmore-cats").show();
    }

    //default topbar position
    jQuery(".header-container.layout1 .top-bar").css("top", (-jQuery(".header-container.layout1 .top-bar").outerHeight()) + "px");
});

function showQuickView(productID) {
    jQuery("#quickview-content").html("");
    window.setTimeout(function () {
        jQuery(".quickview-wrapper").addClass("open");

        jQuery.post(
			ajaxurl,
			{
			    'action': "product_quickview",
			    'data': productID
			},
			function (response) {
			    jQuery("#quickview-content").html(response);
			    jQuery(".quickview-wrapper .quick-modal").addClass("show");
			    /*thumbnails carousel*/
			    jQuery(".quick-thumbnails").addClass("owl-carousel owl-theme");
			    jQuery(".quick-thumbnails").owlCarousel({
			        items: 4,
			        nav: true,
			        rtl: true,
			        dots: true
			    });

			    /* variable product form */
			    if (jQuery("#quickview-content .variations_form").length) {
			        jQuery("#quickview-content .variations_form").wc_variation_form();
			        jQuery("#quickview-content .variations_form .variations select").change();
			    }

			    /*thumbnail click*/
			    jQuery(".quick-thumbnails a").each(function () {
			        var quickThumb = jQuery(this);
			        var quickImgSrc = quickThumb.attr("href");

			        quickThumb.on("click", function (event) {
			            event.preventDefault();

			            jQuery(".main-image").find("img").attr("src", quickImgSrc);
			        });
			    });
			    /*review link click*/

			    jQuery(".woocommerce-review-link").on("click", function (event) {
			        event.preventDefault();
			        var reviewLink = jQuery(".see-all").attr("href");

			        window.location.href = reviewLink + "#reviews";
			    });
			}
		);
    }, 300);
}
function hideQuickView() {
    jQuery(".quickview-wrapper .quick-modal").removeClass("show");
    jQuery(".quickview-wrapper").removeClass("open");
}