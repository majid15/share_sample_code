////////////////////////////////////////////////////////////////////////////////
/////// jQuery  ////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
var automaticGeoLocation = false;
var resizeId;
jQuery(document).ready(function($) {

jQuery( document ).ajaxComplete(function() {
jQuery('select').selectpicker({liveSearch: true, noneResultsText: 'نتیجه‌ای پیدا نشد'});
jQuery('#ad_submit').find('input').iCheck();
});
jQuery('select').selectpicker({liveSearch: true, noneResultsText: 'نتیجه‌ای پیدا نشد'});
jQuery('#content, #sidebar').theiaStickySidebar();
jQuery('#tikpay_payment_form input[type="radio"]').iCheck('disable');
"use strict";

//  Disable inputs in the non-active tab

$(".form-slide:not(.active) input, .form-slide:not(.active) select, .form-slide:not(.active) textarea").prop("disabled", true);


//  Change tab button


    $("select.change-tab").each(function(){
        var _this = $(this);
        if( $(this).find(".item").attr("data-value") !== "" ){
            changeTab( _this );
        }
    });

    $(".change-tab").on("change", function() {
        changeTab( $(this) );
    });

    $(".box").each(function(){
        if( $(this).find(".background .background-image").length ) {
            $(this).css("background-color","transparent");
        }
    });


//  Button for class changing

    $(".change-class").on("click", function(e){
        e.preventDefault();
        var parentClass = $( "."+$(this).attr("data-parent-class") );
        parentClass.removeClass( $(this).attr("data-change-from-class") );
        parentClass.addClass( $(this).attr("data-change-to-class") );
        $(this).parent().find(".change-class").removeClass("active");
        $(this).addClass("active");
        readMore();
    });

    if( $(".masonry").length ){
        $(".items.masonry").masonry({
            itemSelector: ".item",
            transitionDuration: 0
        });
    }

    $(".ribbon-featured").each(function() {
        var thisText = $(this).text();
        $(this).html("");
        $(this).append(
            "<div class='ribbon-start'></div>" +
            "<div class='ribbon-content'>" + thisText + "</div>" +
            "<div class='ribbon-end'>" +
                "<figure class='ribbon-shadow'></figure>" +
            "</div>"
        );
    });

//  File input styling

    if( $("#ad_submit input[type=file].with-preview").length ){
        $("input.file-upload-input").MultiFile({
            list: ".file-upload-previews"
        });
    }

    $(".single-file-input input[type=file]").change(function() {
        previewImage(this);
    });

    $(".has-child a[href='#'].nav-link").on("click", function (e) {
        e.preventDefault();
       if( !$(this).parent().hasClass("hover") ){
           $(this).parent().addClass("hover");
       }
       else {
           $(this).parent().removeClass("hover");
       }
    });

    if( $(".owl-carousel").length ){
        var galleryCarousel = $(".gallery-carousel");

        function setOwlStageHeight(event) {
          jQuery(window).load(function(e){
            var maxHeight = 0;
            $('.owl-item.active').each(function () { // LOOP THROUGH ACTIVE ITEMS
                var thisHeight = parseInt( $(this).height() );
                maxHeight=(maxHeight>=thisHeight?maxHeight:thisHeight);
            });
            $('.owl-carousel').css('height', maxHeight );
            $('.owl-height').css('height', maxHeight );
            $('.owl-stage-outer').css('height', maxHeight ); // CORRECT DRAG-AREA SO BUTTONS ARE CLICKABLE
        });
        }
        function setOwlStageHeight_more(event) {
            var maxHeight = 0;
            $('.owl-item.active').each(function () { // LOOP THROUGH ACTIVE ITEMS
                var thisHeight = parseInt( $(this).height() );
                maxHeight=(maxHeight>=thisHeight?maxHeight:thisHeight);
            });
            $('.owl-carousel').css('height', maxHeight );
            $('.owl-height').css('height', maxHeight );
            $('.owl-stage-outer').css('height', maxHeight ); // CORRECT DRAG-AREA SO BUTTONS ARE CLICKABLE
        }

        $(".tabs-slider").owlCarousel({
            loop: false,
            margin: 0,
            nav: false,
            items: 1,
            autoHeight: true,
            dots: false,
            mouseDrag: true,
            touchDrag: false,
            pullDrag: false,
            freeDrag: false
        });

        $(".full-width-carousel").owlCarousel({
            loop: true,
            margin: 10,
            nav: true,
            items: 3,
            rtl:true,
            navText: ["<i class='fa fa-chevron-left'></i>","<i class='fa fa-chevron-right'></i>"],
            autoHeight: false,
            center: true,
            dots: false,
            autoWidth:true,
            responsive: {
                768: {
                    items: 3
                },
                0 : {
                    items: 1,
                    center: false,
                    margin: 0,
                    autoWidth: false
                }
            }
        });




    }

//  Bootstrap tooltip initialization

    $('[data-toggle="tooltip"]').tooltip();

//  iCheck

    $("input[type=checkbox], input[type=radio]").iCheck();

    var framedInputRadio = $(".framed input[type=radio]");
    framedInputRadio.on('ifChecked', function(){
        $(this).closest(".framed").addClass("active");
    });
    framedInputRadio.on('ifUnchecked', function(){
        $(this).closest(".framed").removeClass("active");
    });

//  "img" into "background-image" transfer

    $("[data-background-image]").each(function() {
        $(this).css("background-image", "url("+ $(this).attr("data-background-image") +")" );
    });

    $(".background-image").each(function() {
        $(this).css("background-image", "url("+ $(this).find("img").attr("src") +")" );
    });

//  Custom background color

    $("[data-background-color]").each(function() {
        $(this).css("background-color", $(this).attr("data-background-color") );
    });

//  No UI Slider -------------------------------------------------------------------------------------------------------

    if( $('.ui-slider').length > 0 ){

        $.getScript( "assets/js/jquery.nouislider.all.min.js", function() {
            $('.ui-slider').each(function() {
                if( $("body").hasClass("rtl") ) var rtl = "rtl";
                else rtl = "ltr";

                var step;
                if( $(this).attr('data-step') ) {
                    step = parseInt( $(this).attr('data-step') );
                }
                else {
                    step = 10;
                }
                var sliderElement = $(this).attr('id');
                var element = $( '#' + sliderElement);
                var valueMin = parseInt( $(this).attr('data-value-min') );
                var valueMax = parseInt( $(this).attr('data-value-max') );
                $(this).noUiSlider({
                    start: [ valueMin, valueMax ],
                    connect: true,
                    direction: rtl,
                    range: {
                        'min': valueMin,
                        'max': valueMax
                    },
                    step: step
                });
                if( $(this).attr('data-value-type') == 'price' ) {
                    if( $(this).attr('data-currency-placement') == 'before' ) {
                        $(this).Link('lower').to( $(this).children('.values').children('.value-min'), null, wNumb({ prefix: $(this).attr('data-currency'), decimals: 0, thousand: '.' }));
                        $(this).Link('upper').to( $(this).children('.values').children('.value-max'), null, wNumb({ prefix: $(this).attr('data-currency'), decimals: 0, thousand: '.' }));
                    }
                    else if( $(this).attr('data-currency-placement') == 'after' ){
                        $(this).Link('lower').to( $(this).children('.values').children('.value-min'), null, wNumb({ postfix: $(this).attr('data-currency'), decimals: 0, thousand: ' ' }));
                        $(this).Link('upper').to( $(this).children('.values').children('.value-max'), null, wNumb({ postfix: $(this).attr('data-currency'), decimals: 0, thousand: ' ' }));
                    }
                }
                else {
                    $(this).Link('lower').to( $(this).children('.values').children('.value-min'), null, wNumb({ decimals: 0 }));
                    $(this).Link('upper').to( $(this).children('.values').children('.value-max'), null, wNumb({ decimals: 0 }));
                }
            });
        });
    }

//  Read More

    readMore();

});



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Functions
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


// Change Tab

function changeTab(_this){
    var parameters = _this.data("selectize").items[0];
    var changeTarget = $("#" + _this.attr("data-change-tab-target"));
    var slide = changeTarget.find(".form-slide");
    if( parameters === "" ){
        slide.removeClass("active");
        slide.first().addClass("default");
        changeTarget.find("input").prop("disabled", true);
        changeTarget.find("select").prop("disabled", true);
        changeTarget.find("textarea").prop("disabled", true);
    }
    else {
        slide.removeClass("default");
        slide.removeClass("active");
        changeTarget.find("input").prop("disabled", true);
        changeTarget.find("select").prop("disabled", true);
        changeTarget.find("textarea").prop("disabled", true);
        changeTarget.find( "#" + parameters ).addClass("active");
        changeTarget.find( "#" + parameters + " input").prop("disabled", false);
        changeTarget.find( "#" + parameters + " textarea").prop("disabled", false);
        changeTarget.find( "#" + parameters + " select").prop("disabled", false);
    }
}

// Read More

function readMore() {
    jQuery(".read-more").each(function(){
        var readMoreLink = $(this).attr("data-read-more-link-more");
        var readLessLink = $(this).attr("data-read-more-link-less");
        var collapseHeight = $(this).find(".item:first").height() + parseInt( $(this).find(".item:first").css("margin-bottom"), 10 );
        jQuery(".read-more").readmore({
            moreLink: '<div class="center"><a href="#" class="btn btn-primary btn-rounded btn-framed">' + readMoreLink + '</a></div>',
            lessLink: '<div class="center"><a href="#" class="btn btn-primary btn-rounded btn-framed">' + readLessLink + '</a></div>',
            collapsedHeight: 500
        });
    });
}



( function( $ ) {
$( document ).ready(function() {

$('.vertical-menu .dropdown-submenu').on('click', function(){
  if($(this).find('.dropdown-child').hasClass('show')){
    $(this).find('.children').slideUp();
    $(this).find('.dropdown-child').removeClass('show');
  }else{
  $('.children').slideUp();
  $('.dropdown-child').removeClass('show');
  $(this).find('.children').slideDown();
  $(this).find('.dropdown-child').addClass('show');
  }
});

$('#cssmenu li.has-sub>a').on('click', function(){
		$(this).removeAttr('href');
		var element = $(this).parent('li');
		if (element.hasClass('open')) {
			element.removeClass('open');
			element.find('li').removeClass('open');
			element.find('ul').slideUp();
		}
		else {
			element.addClass('open');
			element.children('ul').slideDown();
			element.siblings('li').children('ul').slideUp();
			element.siblings('li').removeClass('open');
			element.siblings('li').find('li').removeClass('open');
			element.siblings('li').find('ul').slideUp();
		}
	});

	$('#cssmenu>ul>li.has-sub>a').append('<span class="holder"></span>');


	function rgbToHsl(r, g, b) {
	    r /= 255, g /= 255, b /= 255;
	    var max = Math.max(r, g, b), min = Math.min(r, g, b);
	    var h, s, l = (max + min) / 2;

	    if(max == min){
	        h = s = 0;
	    }
	    else {
	        var d = max - min;
	        s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
	        switch(max){
	            case r: h = (g - b) / d + (g < b ? 6 : 0); break;
	            case g: h = (b - r) / d + 2; break;
	            case b: h = (r - g) / d + 4; break;
	        }
	        h /= 6;
	    }
	    return l;
	}
});
} )( jQuery );
