<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvertisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advertises', function (Blueprint $table) {
            $table->bigIncrements('id');


            $table->unsignedBigInteger('user_id'); //Forign key to user

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');

            $table->unsignedBigInteger('job_id'); //Forign key to job

            $table->foreign('job_id')
                ->references('id')->on('jobs')
                ->onDelete('cascade');

            $table->unsignedBigInteger('sub_job_id'); //Forign key to sub_job

            $table->foreign('sub_job_id')
                ->references('id')->on('sub_jobs')
                ->onDelete('cascade');

            $table->unsignedBigInteger('country_id'); //Forign key to country

            $table->foreign('country_id')
                ->references('id')->on('countries')
                ->onDelete('cascade');

            $table->unsignedBigInteger('state_id'); //Forign key to state

            $table->foreign('state_id')
                ->references('id')->on('states')
                ->onDelete('cascade');


            $table->unsignedBigInteger('city_id'); //Forign key to city

            $table->foreign('city_id')
                ->references('id')->on('cities')
                ->onDelete('cascade');

            $table->integer('status')->default(0);
            $table->string('statustag_fory')->default(null)->nullable();
            $table->string('statustag_vizheh')->default(null)->nullable();
            $table->string('statustag_top')->default(null)->nullable();
            $table->string('statustag_nardeban')->default(null)->nullable();
            $table->string('title_ads')->default(null)->nullable();
            $table->text('description_ads')->default(null)->nullable();
            $table->string('grade')->default(null)->nullable();
            $table->string('education')->default(null)->nullable();
            $table->string('historywork')->default(null)->nullable();
            $table->integer('minage')->default(null)->nullable();
            $table->integer('maxage')->default(null)->nullable();

            $table->string('email_contact')->default(null)->nullable();
            $table->string('phone_contact')->default(null)->nullable();
            $table->string('message_contact')->default(null)->nullable();
            $table->string('whatsapp_contact')->default(null)->nullable();

            $table->string('married')->default(null)->nullable();
            $table->string('soldier')->default(null)->nullable();
            $table->string('kind1')->default(null)->nullable();
            $table->string('kind2')->default(null)->nullable();
            $table->string('kind3')->default(null)->nullable();
            $table->string('kind4')->default(null)->nullable();
            $table->string('remote')->default(null)->nullable();
            $table->integer('minsalary')->default(null)->nullable();
            $table->integer('maxsalary')->default(null)->nullable();

            $table->string('kindpeyment')->default(null)->nullable();
            $table->string('bimeh')->default(null)->nullable();

            $table->string('price_payment')->default(null)->nullable();
            $table->integer('status_peyment')->default(0);


            $table->string('slug')->default(null)->nullable();
            $table->string('keyword')->default(null)->nullable();
            $table->string('url')->default(null)->nullable();



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('advertises');
    }
}
