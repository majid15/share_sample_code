<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCvsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cvs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id'); //Forign key to user

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
				
				
            $table->string('title')->default(null)->nullable();
            $table->integer('showcv')->default(1);
            $table->string('grade')->default(null)->nullable();
            $table->string('married')->default(null)->nullable();
            $table->string('country')->default(null)->nullable();
            $table->string('state')->default(null)->nullable();
            $table->string('city')->default(null)->nullable();
            $table->string('soldier')->default(null)->nullable();
            $table->integer('age')->default(null)->nullable();;
            $table->string('education')->default(null)->nullable();
            $table->integer('salary')->default(null)->nullable();;
            $table->string('kind1')->default(null)->nullable();
            $table->string('kind2')->default(null)->nullable();
            $table->string('kind3')->default(null)->nullable();
            $table->string('kind4')->default(null)->nullable();
            $table->text('description')->default(null)->nullable();


            $table->string('slug')->default(null)->nullable();
            $table->string('keyword')->default(null)->nullable();
            $table->string('url')->default(null)->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cvs');
    }
}
