<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShowcvsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('showcvs', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('user_id'); //Forign key to user

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');


            $table->unsignedBigInteger('job_id');  //Forign key to job

            $table->foreign('job_id')
                ->references('id')->on('jobs')
                ->onDelete('cascade');


            $table->unsignedBigInteger('cv_id'); //Forign key to cv

            $table->foreign('cv_id')
                ->references('id')->on('cvs')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('showcvs');
    }
}
