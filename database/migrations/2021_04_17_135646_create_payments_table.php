<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->bigIncrements('id');


            $table->unsignedBigInteger('user_id'); //changed this line

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');

            $table->integer('status')->default(0)->nullable();
            $table->integer('price')->default(null)->nullable();
            /**
            برای آگهی
             */

            $table->integer('advertise_id')->default(null)->nullable();
            $table->integer('price_ads')->default(null)->nullable();
            $table->integer('price_fory')->default(null)->nullable();
            $table->integer('price_vizheh')->default(null)->nullable();
            $table->integer('price_top')->default(null)->nullable();
            $table->integer('price_nardeban')->default(null)->nullable();
            $table->integer('price_fory_nardeban')->default(null)->nullable();
            $table->integer('price_tamdid_nardeban')->default(null)->nullable();



            /**
           برای مشاهده رزومه
             */

            $table->integer('tedad')->default(null)->nullable();

            $table->string('description')->default(null)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
