<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Advertise;
use Faker\Generator as Faker;



$factory->define(Advertise::class, function (Faker $faker) {

    $users = App\User::pluck('id')->toArray();
    $jobs = App\Job::pluck('id')->toArray();
    $subjobs = App\SubJob::pluck('id')->toArray();
    $countrys = App\Country::pluck('id')->toArray();
    $states = App\State::pluck('id')->toArray();
    $citys = App\City::pluck('id')->toArray();

    return [
        'user_id' => $faker->randomElement($users),
        'job_id' => $faker->randomElement($jobs),
        'sub_job_id' => $faker->randomElement($subjobs),
        'country_id' => $faker->randomElement($countrys),
        'state_id' => $faker->randomElement($states),
        'city_id' => $faker->randomElement($citys),
        'title_ads' => $faker->sentence($nbWords = 2, $variableNbWords = true), // Random task description
        'description_ads' => $faker->text(), // Random task description
        'grade' => $faker->sentence($nbWords = 1, $variableNbWords = true), // Random task description
        'historywork' => $faker->sentence($nbWords = 1, $variableNbWords = true), // Random task description
        'minage' => $faker->numerify('##'), // Random task description
        'maxage' => $faker->numerify('##'), // Random task description
        'email_contact' => preg_replace('/@example\..*/', '@domain.com', $faker->unique()->safeEmail), // Random task description
        'whatsapp_contact' => $faker->numerify('##########'), // Random task description
        'message_contact' => $faker->numerify('##########'), // Random task description
        'married' => $faker->sentence($nbWords = 1, $variableNbWords = true), // Random task description
        'soldier' => $faker->sentence($nbWords = 1, $variableNbWords = true), // Random task description
        'kind1' => $faker->sentence($nbWords = 1, $variableNbWords = true), // Random task description
        'kind2' => $faker->sentence($nbWords = 1, $variableNbWords = true), // Random task description
        'kind3' => $faker->sentence($nbWords = 1, $variableNbWords = true), // Random task description
        'kind4' => $faker->sentence($nbWords = 1, $variableNbWords = true), // Random task description
        'remote' => $faker->sentence($nbWords = 1, $variableNbWords = true), // Random task description

        'minsalary' => $faker->numerify('######'), // Random task description
        'maxsalary' => $faker->numerify('######'), // Random task description
        'bimeh' =>  $faker->sentence($nbWords = 1, $variableNbWords = true), // Random task description
        'kindpeyment' =>  $faker->sentence($nbWords = 1, $variableNbWords = true), // Random task description
        'url' =>  $this->faker->unique()->numberBetween(1, 100000000),
        'keyword' =>  $faker->sentence($nbWords = 5, $variableNbWords = true), // Random task description


    ];
});
